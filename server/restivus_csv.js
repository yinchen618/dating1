/*if(Agents.find().count == 0){
  Agents.insert({value:"1", text:"Aaron"})
}*/


objAddrEng = [
  {id: 1, cht:"市", eng:"City"},
  {id: 2, cht:"縣", eng:"County"},
  {id: 3, cht:"鄉", eng:"Township"},
  {id: 4, cht:"鎮", eng:"Township"},
  {id: 5, cht:"區", eng:"Dist."},
  {id: 6, cht:"里", eng:"Vil."},
  {id: 7, cht:"村", eng:"Vil."},
  {id: 8, cht:"鄰", eng:"Neighborhood"},
  {id: 9, cht:"路", eng:"Rd."},
  {id: 10, cht:"街", eng:"St."},
  {id: 11, cht:"大道", eng:"Blvd."},
  {id: 20, cht:"段", eng:"sec."},
  {id: 21, cht:"巷", eng:"Ln."},
  {id: 22, cht:"弄", eng:"Aly."},
  {id: 23, cht:"衖", eng:"Sub-Alley"},
  {id: 24, cht:"號", eng:"No."},
  {id: 25, cht:"樓", eng:"F"},
  {id: 26, cht:"室", eng:"Rm."},
  {id: 27, cht:"東", eng:"E."},
  {id: 28, cht:"西", eng:"W."},
  {id: 29, cht:"南", eng:"S."},
  {id: 30, cht:"北", eng:"N."},
  {id: 31, cht:"一", eng:"1st"},
  {id: 32, cht:"二", eng:"2nd"},
  {id: 33, cht:"三", eng:"3rd"},
  {id: 34, cht:"四", eng:"4th"},
  {id: 35, cht:"五", eng:"5th"},
  {id: 36, cht:"六", eng:"6th"},
  {id: 37, cht:"七", eng:"7th"},
  {id: 38, cht:"八", eng:"8th"},
  {id: 39, cht:"九", eng:"9th"},
  {id: 40, cht:"十", eng:"10th"},
];

Api.addRoute('createaddrdb', {authRequired: false}, {
  get: function () {
  /*
  post,cht,eng
  100,臺北市中正區,"Zhongzheng Dist., Taipei City"
   */
    var filestr = Assets.getText('county_h_10410_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];
      Addr1.insert(data);
      // console.log(data);

    }
    console.log("addr1 ok");

    var filestr = Assets.getText('Village_H_10409_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];
      Addr2.insert(data);
      // console.log(data);
    }
    console.log("addr2 ok");

    /*
    cht,eng
    一中街,Yizhong St.
     */
    var filestr = Assets.getText('CE_Rd_St_H_10409_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];
      Addr3.insert(data);
      // console.log(data);
    }
    console.log("addr3 ok");
    return "";
  }
});

Api.addRoute('workdays_data', {authRequired: false}, {
  get: function () {
    var filestr = Assets.getText('work_days.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    if(Workdays.find().count() != 0){
      return "資料庫非空的 取消新增";
    }
    for (var i=0; i<myArray.length; i++) {
      var data = myArray[i];
      data.order_id = i;

      Workdays.insert(data);
      console.log(data);
    }
    console.log("workdays ok");
    return "workdays ok";
  }
});
Api.addRoute('chgCellnumInit0', {authRequired: false}, {
  get: function () {

    var arr = Clients.find().fetch();
    for (var i = 0; i < arr.length; i++) {
      var entry = arr[i];

      var new_cellnum = entry.cellnum;
      if(!!entry && !!entry.cellnum && entry.cellnum.charAt(0) == "0"){
        new_cellnum = entry.cellnum.replace("0", "+886-")

        console.log("old: "+entry.cellnum+" new: "+new_cellnum );
      }
      // if(!!entry && !!entry.cellnum && entry.cellnum.indexOf("+886")!= -1 ){
      //   new_cellnum = entry.cellnum.replace("+886", "+886-")

      //   console.log("old: "+entry.cellnum+" new: "+new_cellnum );
      // }

      var params = {};
      params.cellnum = new_cellnum;
      Clients.update(entry._id, {$set: params });
    }
    return "";
  }
});
Api.addRoute('createoproduct3', {authRequired: false}, {
  get: function () {

    var arr = Oproduct3.find().fetch();
    for (var i = 0; i < arr.length; i++) {
      var entry = arr[i];

      var p4 = Oproduct4.findOne({product3_id: entry._id}, {sort: {order_id: 1}});

      console.log("p4");
      console.log(p4);

      var params = {};
      params.product4_order1_id = p4._id;
      Oproduct3.update(entry._id, {$set: params });
    }
    return "";
  }
});
Api.addRoute('createaddrdb2', {authRequired: false}, {
  get: function () {
  /*
  post5,addr1,addr2,addr3,addr4
  10058,台北市,中正區,八德路一段,全
   */
    var filestr = Assets.getText('Zip32_csv_10501_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      var post3 = data.post5.substr(0, 3);
      if(Addr1a.find({addr1: data.addr1, addr2: data.addr2}).count() == 0){
        var obj1 = {
          post3: post3,
          addr1: data.addr1,
          addr2: data.addr2
        }
        Addr1a.insert(obj1);
      }

      Addr2a.insert(data);
      console.log(data.post5 + " " + data.addr1 + " " + data.addr2 + " " + data.addr3 + " " + data.addr4);

    }
    console.log("addr1a 2a ok");
    return "";
  }
});
Api.addRoute('createaccount', {authRequired: false}, {
  get: function () {
    var filestr = Assets.getText('account.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      var c = Account.find({value: data.value});
      var id1 = "";
      if(c.count() == 0){
        id1 = Account.insert({order_id: Number(i), value: data.value});
      }

      console.log(data);

    }
    console.log("account ok");
    return "account ok";
  }
});
Api.addRoute('createbankacc', {authRequired: false}, {
  get: function () {
    var filestr = Assets.getText('bankacc.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      var c = Bankacc.find({value: data.value});
      var id1 = "";
      if(c.count() == 0){
        id1 = Bankacc.insert({order_id: Number(i), value: data.value});
      }

      console.log(data);

    }
    console.log("Bankacc ok");
    return "Bankacc ok";
  }
});
Api.addRoute('createaccount12', {authRequired: false}, {
  get: function () {
/*
post,cht,eng
100,臺北市中正區,"Zhongzheng Dist., Taipei City"
 */
    var filestr = Assets.getText('account12.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      var c = Account1.find({value: data.account1});
      var id1 = "";
      if(c.count() == 0){
        id1 = Account1.insert({value: data.account1});
      }
      else{
        id1 = c.fetch()[0]._id;
      }

      Account2.insert({
        parent_id: id1,
        value: data.account2
      });

      console.log(data);

    }
    console.log("account ok");
    return "account 1 2 ok";
  }
});

Api.addRoute('createproducts', {authRequired: false}, {
  get: function () {
  /*
  1
  value,id
  投資,1

  2
  name_cht,name_eng,product2_id,product1_id
  投資型保單,Variable Account,1,1
  */
    var filestr = Assets.getText('product1_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    // if(0)
    for (var i in myArray) {
      var data = myArray[i];

      if(!Product1.find({value: data.value}).count())
      Product1.insert({
        product1_oid: data.id,
        value: data.value,
      });
    }
    console.log("product1_utf8 ok");

    var filestr = Assets.getText('product2_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    // if(0)
    for (var i in myArray) {
      var data = myArray[i];

      var pObj = Product1.findOne({product1_oid: data.product1_id});
      if(!!pObj){
        if(!Product2.find({name_cht: data.name_cht}).count())
        Product2.insert({
          product1_id: pObj._id,
          product1_oid: pObj.product1_oid,
          product1_text: pObj.value,
          product2_oid: data.product2_id,
          name_cht: data.name_cht,
          name_eng: data.name_eng,
        });
      }
    }
    console.log("product2_utf8 ok");

    /*
    3
    name_cht,name_eng,product3 id,product2 id,product1 id
    本金履約保本型,PRINCIPAL PROTECTION,1,1,1

    4
    name_cht,name_eng,product4_id,product3_id,product2_id,product1_id
    史坦普500指數保本型計畫,S&P 500 Index,1,1,1,1
    */
    var filestr = Assets.getText('product3_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    // if(0)
    for (var i in myArray) {
      var data = myArray[i];

      var pObj = Product2.findOne({product2_oid: data.product2_id});
      if(!!pObj){
        if(!Product3.find({name_cht: data.name_cht}).count())
        Product3.insert({
          product1_id: pObj.product1_id,
          product1_oid: pObj.product1_oid,
          product1_text: pObj.product1_text,
          product2_id: pObj._id,
          product2_oid: pObj.product2_oid,
          product2_text: pObj.name_cht,
          product3_oid: data.product3_id,
          name_cht: data.name_cht,
          name_eng: data.name_eng,
        });
      }
    }
    console.log("product3_utf8 ok");

    var filestr = Assets.getText('product4_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      var pObj = Product3.findOne({product3_oid: data.product3_id});
      if(!!pObj){
        if(!Product4.find({name_cht: data.name_cht}).count()){
          var provider = Provider.findOne({provider_oid: data.provider_id});
          Product4.insert({
            product1_id: pObj.product1_id,
            product1_oid: pObj.product1_oid,
            product1_text: pObj.product1_text,
            product2_id: pObj.product2_id,
            product2_oid: pObj.product2_oid,
            product2_text: pObj.product2_text,
            product3_id: pObj._id,
            product3_oid: pObj.product3_oid,
            product3_text: pObj.name_cht,
            product4_oid: data.product4_id,
            name_cht: data.name_cht,
            name_eng: data.name_eng,
            is_newcase: data.is_newcase,
            provider_id: provider._id,
            provider_chttext: provider.name_cht,
            provider_engtext: provider.name_eng,
            template_id: data.template_id,
          });
        }
      }
    }
    console.log("product4_utf8 ok");
    return "product ok";
  }
});
Api.addRoute('createoproducts', {authRequired: false}, {
  get: function () {
  /*
product1,product2,product3,product4_cht,product4_eng,
房地產,美國房產,安養院,品達歌德頤養園,"Punta Gorda Senior Living, LP",
  */
    var filestr = Assets.getText('oproduct_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    var c1 = c2 = c3= c4 = 1;
    for (var i in myArray) {
      var data = myArray[i];
      console.log(data);

      var p1 = Oproduct1.findOne({value: data.product1});
      var p1_id = "";
      if(!p1){
        p1_id = Oproduct1.insert({
          order_id: c1,
          value: data.product1,
        });
        c1++;
        p1 = Oproduct1.findOne({value: data.product1})
      }
      else{
        p1_id = p1._id;
      }
      var p2 = Oproduct2.findOne({value: data.product2});
      var p2_id = "";
      if(!p2){
        p2_id = Oproduct2.insert({
          product1_id: p1_id,
          product1_text: p1.value,
          order_id: c2,
          value: data.product2,
        });
        c2++;
        p2 = Oproduct2.findOne({value: data.product2});
      }
      else{
        p2_id = p2._id;
      }
      var p3 = Oproduct3.findOne({value: data.product3});
      var p3_id = "";
      if(!p3){
        p3_id = Oproduct3.insert({
          product1_id: p1_id,
          product1_text: p1.value,
          product2_id: p2_id,
          product2_text: p2.value,
          order_id: c3,
          value: data.product3,
        });
        c3++;
        p3 = Oproduct3.findOne({value: data.product3});
      }
      else{
        p3_id = p3._id;
      }
      var p4 = Oproduct4.findOne({product4: data.product4_cht});
      var p4_id = "";
      if(!p4){
        p4_id = Oproduct4.insert({
          product1_id: p1_id,
          product1_text: p1.value,
          product2_id: p2_id,
          product2_text: p2.value,
          product3_id: p3_id,
          product3_text: p3.value,
          order_id: c4,
          value_cht: data.product4_cht,
          value_eng: data.product4_eng,
        });
        c4++;
      }
      else{
        p4_id = p4._id;
      }

    }
    console.log("product1_utf8 ok");
    return "product ok";
  }
});


Api.addRoute('createproducteng', {authRequired: false}, {
  get: function () {
  /*
name_cht,name_eng,provider_id
ITA,ITA,1
  */
/*    var filestr = Assets.getText('provider_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      Provider.insert({
        provider_oid: data.provider_id,
        name_cht: data.name_cht,
        name_eng: data.name_eng,
      });
    }*/

    // if(0){ // ok
    // var i=0, obj;
    Portfolios.find().forEach(function(u){
      var p3_eng = Product3.findOne(u.product3_id).name_eng;
      var p4_eng = Product4.findOne(u.product4_id).name_eng;
      Portfolios.update(u, {$set:{
        "product3_engtext" :  p3_eng,
        "product4_engtext" :  p4_eng
      }});

      console.log("Portfolios " + u.name_cht +" "+u.product4_text+" "+p3_eng+" "+p4_eng);
    });
    return "provider ok";
  }
});
Api.addRoute('createprovider', {authRequired: false}, {
  get: function () {
  /*
name_cht,name_eng,provider_id
ITA,ITA,1
  */
    var filestr = Assets.getText('provider_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      Provider.insert({
        provider_oid: data.provider_id,
        name_cht: data.name_cht,
        name_eng: data.name_eng,
      });
    }
    return "provider ok";
  }
});

Api.addRoute('createagent', {authRequired: false}, {
  get: function () {
  /*
name,id
Aaron,1
  */
    var filestr = Assets.getText('agent.csv');
    var papa = Papa.parse(filestr, {header: true});

    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];

      Agents.insert({
        agent_id: data.id,
        name: data.name,
      });
    }
    return "Agents ok";
  }
});

Api.addRoute('createdb', {authRequired: false}, {
  get: function () {
    /*
    agent_id,name_cht,client_id,name_eng,email,birthday,title_text,,sexual_text,
    identify,passport,,addr1_phone,cellnum,addr1_post5,addr1_cht,addr_eng,fin_company_cht,fin_addr_cht,addr2_cht
    Aaron,云天飛,1,"Yun, Tian-Fei",228876948@qq.com,1976/4/17,,中國,男,
    220202197604173639,G55826889,,057186959116,15968-163-413,,杭州市江干區觀瀾時代國際花園雲邸5幢1單元3201室,,,,
     */

    var filestr = Assets.getText('client_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    // if(0)
    for (var i in myArray) {
      var data = myArray[i];
      var id = "";

      var uid = "";
      // console.log(data.agent_id);
      var agentid = Agents.findOne({name: data.agent_id})._id;

      if(Clients.find({name_cht: data.name_cht}).count() == 0){ // 如果沒有這個人
        uid = "H"+funcStrPad(getNextSequence('clients'), 7);
        var addr_eng = "";
       /* if(!data.addr_eng && !!data.addr1_cht){
          addr_eng = Meteor.call('transAddrChtEng', data.addr1_cht);
        }*/

        // var title_id = "";
        // for(var j in objTitle){
        //   if(objTitle[j].value == data.title_text){
        //    title_id = objTitle[j].id;
        //   }
        // }

        id = Clients.insert({
          uid: uid,
          client_oid: data.client_id,
          review_id: "1",
          review_text: "已建檔",
          name_cht: data.name_cht,
          name_eng: data.name_eng,
          sexual_text: data.sexual_text,
          sexual_id: (data.sexual_text=="男")?"1":"2",
          title_text: data.title_text,
          // title_id: title_id,
          birthday: data.birthday,
          identify: data.identify,
          passport: data.passport,
          marriage_id:"",
          marriage_text:"",
          // phonenum: data.phonenum,
          cellnum: data.cellnum,
          faxnum: "",
          country_text: data.country_text,
          addr1_phone: data.addr1_phone,
          addr1_post5: data.addr1_post5,
          addr1_cht: data.addr1_cht,
          addr1_eng: addr_eng,
          addr2_phone: "",
          addr2_post5: "",
          addr2_cht: data.addr2_cht,
          addr2_eng: "",
          email: data.email,
          fin_type: "",
          fin_company_cht: data.fin_company_cht,
          fin_company_eng: "",
          fin_addr_cht: data.fin_addr_cht,
          fin_addr_eng: "",
          fin_yearsalary: "",
          fin_invexp_text: "",
          fin_purpose_text: "",
          ps: "系統匯入",
          agent_id: agentid,
          agent_text: data.agent_id,
          review0_total_sec: 0,
          review1_total_sec: 0,
          review2_total_sec: 0,
          review3_total_sec: 0,
          review4_total_sec: 0,
          review5_total_sec: 0,
          review6_total_sec: 0,
          review7_total_sec: 0,
          review8_total_sec: 0,
          review0_start_time: "",
          review1_start_time: new Date(),
          review2_start_time: "",
          review3_start_time: "",
          review4_start_time: "",
          review5_start_time: "",
          review6_start_time: "",
          review7_start_time: "",
          review8_start_time: "",
          review0_end_time: "",
          review1_end_time: "",
          review2_end_time: "",
          review3_end_time: "",
          review4_end_time: "",
          review5_end_time: "",
          review6_end_time: "",
          review7_end_time: "",
          review8_end_time: "",
          review0_by: "",
          review1_by: "",
          review2_by: "",
          review3_by: "",
          review4_by: "",
          review5_by: "",
          review6_by: "",
          review7_by: "",
          review8_by: "",
          review0_name: "",
          review1_name: "Michael",
          review2_name: "",
          review3_name: "",
          review4_name: "",
          review5_name: "",
          review6_name: "",
          review7_name: "",
          review8_name: "",
          review_count: 0,
          insertedBy: "aaa",
          insertedName: "Michael",
          insertedAt: new Date(),
          // updatedBy: "aaa",
          // updatedName: "Michael",
          // updatedAt: new Date(),
        });

        var obj2 = {
          // uid: "h"+funcStrPad(getNextSequence("h"+uid), 3),
          parent_id: id, //formVar._id,
          from_review_id: "0",
          from_review_text: "草稿",
          to_review_id: "1",
          to_review_text: "已建檔",
          interval: 0,
          review_count: 0,
          insertedBy: "aaa",
          insertedName: "Michael",
          insertedAt: new Date(),
          ps: "系統匯入",
          p_name_cht: data.name_cht,
          p_uid: uid,
        }
        Review1.insert(obj2);
      }
      else{
        var c = Clients.find({name_cht: data.name_cht}).fetch()[0];
        id = c._id;
        uid = c.uid;
      }
      console.log(id + " " + data.name_cht);
    }

    var filestr = Assets.getText('portfolio_utf8.csv');
    var papa = Papa.parse(filestr, {header: true});
    var myArray = papa.data;
    for (var i in myArray) {
      var data = myArray[i];
      /*
    { id:"1", value:"房產"},
    { id:"2", value:"安養院"},
    { id:"3", value:"保險"},
    { id:"4", value:"第三方資產管理"},
    { id:"5", value:"投資"}
      - R 房產
      - A 安養院
      - I 保險
      - E 第三方資產管理 (?)
      - V 投資 (?)
       */

      var uid = "";
      var product1_id = "";
   /*   if(data.product1 == "房產"){
        uid = "R"+funcStrPad(getNextSequence('portfolio_r'), 7);
        product1_id = "1";
      }
      else if(data.product1 == "安養院"){
        uid = "A"+funcStrPad(getNextSequence('portfolio_a'), 7);
        product1_id = "2";
      }
      else if(data.product1 == "保險"){
        uid = "I"+funcStrPad(getNextSequence('portfolio_i'), 7);
        product1_id = "3";
      }
      else if(data.product1 == "第三方資產管理"){
        uid = "E"+funcStrPad(getNextSequence('portfolio_e'), 7);
        product1_id = "4";
      }
      else if(data.product1 == "投資"){
        uid = "V"+funcStrPad(getNextSequence('portfolio_v'), 7);
        product1_id = "5";
      }*/

      var c = Clients.findOne({client_oid: data.client_id});
      var p4 = Product4.findOne({product4_oid: data.product4_id});

      // console.log(data);
      // console.log(c);
      // console.log(p4);
      if(!c || !p4){
        console.log("cannot find client or product4");
        console.log(data);
        console.log(c);
        console.log(p4);
      }

      if(Portfolios.find({oid:data.oid, account_num: data.account_num}).count()){
        console.log("duplicate: " + c.name_cht +" "+p4.name_cht);
        continue;
      }
      uid = "P"+funcStrPad(getNextSequence('portfolio_p'), 7);

      var id2 = Portfolios.insert({
        uid: uid,
        client_id: c._id,
        client_uid: c.uid,
        client_oid: data.client_id,
        review_id: "1",
        review_text: "已建檔",
        oid: data.oid,
        product1_id: p4.product1_id,
        product1_text: p4.product1_text,
        product2_id: p4.product2_id,
        product2_text: p4.product2_text,
        product3_id: p4.product3_id,
        product3_text: p4.product3_text,
        product4_id: p4._id,
        product4_text: p4.name_cht,
        name_cht: c.name_cht,
        name_eng: c.name_eng,
        contactnum: c.cellnum || c.addr1_phone,
        email: c.email,
        account_num: data.account_num,
        fpi_num: data.fpi_num,

        provider_id : p4.provider_id,
        provider_chttext : p4.provider_chttext,
        provider_engtext : p4.provider_engtext,
        template_id : p4.template_id,
        // start_date: data.start_date,
        // invest_money: data.invest_money,
        // prod_money: data.prod_money,
        // prod_apply_book: data.prod_apply_book,
        // prod_healthy_check: data.prod_healthy_check,
        // prod_tele_invest: data.prod_tele_invest,
        // prod_remit_indicate: data.prod_remit_indicate,
        // prod_remit_date: data.prod_remit_date,
        // prod_fund_receipt: data.prod_fund_receipt,
        // prod_certificate_soft_copy: data.prod_certificate_soft_copy,
        // prod_customer_recv_sign: data.prod_customer_recv_sign,
        // prod_new_case_ps: data.prod_new_case_ps,
        // fpi_plan: data.fpi_plan,
        // fpi_start_period: data.fpi_start_period,
        // fpi_first_price: data.fpi_first_price,
        // fpi_now_period: data.fpi_now_period,
        // fpi_period_money: data.fpi_period_money,
        // fpi_period_year: data.fpi_period_year,
        // fpi_account_status: data.fpi_account_status,
        // fpi_pay_status: data.fpi_pay_status,
        // fpi_send_method: data.fpi_send_method,
        // fpi_beneficiaries_cht: data.fpi_beneficiaries_cht,
        // fpi_report_sent_day: data.fpi_report_sent_day,
        // voya_2011: data.voya_2011,
        // voya_2012: data.voya_2012,
        // voya_2013: data.voya_2013,
        // voya_2014: data.voya_2014,
        // voya_2015: data.voya_2015,
        // voya_2016: data.voya_2016,
        // provider_id 這兩個要再加
        // provider_text
        ps: "系統匯入", // data.ps || "",
        agent_id: c.agent_id || "",
        agent_text: c.agent_text || "",
        review0_total_sec: 0,
        review1_total_sec: 0,
        review2_total_sec: 0,
        review3_total_sec: 0,
        review4_total_sec: 0,
        review5_total_sec: 0,
        review6_total_sec: 0,
        review7_total_sec: 0,
        review8_total_sec: 0,
        review0_start_time: "",
        review1_start_time: new Date(),
        review2_start_time: "",
        review3_start_time: "",
        review4_start_time: "",
        review5_start_time: "",
        review6_start_time: "",
        review7_start_time: "",
        review8_start_time: "",
        review0_end_time: "",
        review1_end_time: "",
        review2_end_time: "",
        review3_end_time: "",
        review4_end_time: "",
        review5_end_time: "",
        review6_end_time: "",
        review7_end_time: "",
        review8_end_time: "",
        review0_by: "",
        review1_by: "",
        review2_by: "",
        review3_by: "",
        review4_by: "",
        review5_by: "",
        review6_by: "",
        review7_by: "",
        review8_by: "",
        review0_name: "",
        review1_name: "Michael",
        review2_name: "",
        review3_name: "",
        review4_name: "",
        review5_name: "",
        review6_name: "",
        review7_name: "",
        review8_name: "",
        review_count: 0,
        // createdBy: "aaa",
        // createdName: "Michael",
        // createdAt: new Date(),
        insertedBy: "aaa",
        insertedName: "Michael",
        insertedAt: new Date(),
      });
      console.log(i+" " +c.name_cht + " " + p4.name_cht);

      var obj = {
        parent_id: id2,
        from_review_id: "0",
        from_review_text: "草稿",
        to_review_id: "1",
        to_review_text: "已建檔",
        interval: 0,
        review_count: 0,
        insertedBy: "aaa",
        insertedName: "Michael",
        insertedAt: new Date(),
        ps: "系統匯入",
        p_uid: uid,
        p_name_cht: data.name_cht,
        // p_product: data.product1+"-"+data.product2,
      }
      Review2.insert(obj);
      // console.log(data);

    }
    console.log("db ok");
    return "";
  }
});

