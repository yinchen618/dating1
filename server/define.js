basePath = process.cwd() + '/../web.browser/app/';

// dpi 220
as1_1 = basePath+'afterservice/FPI_1_1.png';
as1_2 = basePath+'afterservice/FPI_1_2.png';
as1_3 = basePath+'afterservice/FPI_1_3.png';
as1_4 = basePath+'afterservice/FPI_1_4.png';

wd_1 = basePath+'afterservice/WD/WithdrawalDocument2_1.png';
wd_2 = basePath+'afterservice/WD/WithdrawalDocument2_2.png';
wd_3 = basePath+'afterservice/WD/WithdrawalDocument2_3.png';
wd_4 = basePath+'afterservice/WD/WithdrawalDocument2_4.png';
wd_5 = basePath+'afterservice/WD/WithdrawalDocument2_5.png';
wd_6 = basePath+'afterservice/WD/WithdrawalDocument2_6.png';
wd_7 = basePath+'afterservice/WD/WithdrawalDocument2_7.png';
wd_8 = basePath+'afterservice/WD/WithdrawalDocument2_8.png';

paidUp = basePath+'afterservice/FPI-Paid-UpFormbilingual.png';

premium = basePath+'afterservice/FPI-PremiumHolidayFormbilingual.png';

Reinstatement = basePath+'afterservice/FPI-ReinstatementFormbilingual_2015.png';

blank = basePath+'afterservice/FPI_blank.png';

BSO = basePath+'afterservice/FPI-BSOChi&Eng.png';

CancelBSO = basePath+'afterservice/FPI-InstructiontoCancelBSOform.png';

NEW_1 = basePath+'afterservice/FPI_NEW_1.png';
NEW_2 = basePath+'afterservice/FPI_NEW_2.png';


// VOYA_WF = basePath+'afterservice/Voya_Wells_Fargo.png';
VOYA_WF = basePath+'afterservice/Voya_Wells_Fargo_2.png';

gt_1 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_1.png';
gt_2 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_2.png';
gt_3 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_3.png';
gt_4 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_4.png';
gt_5 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_5.png';
gt_6 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_6.png';
gt_7 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_7.png';
gt_8 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_8.png';
gt_9 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_9.png';
gt_10 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_10.png';
gt_11 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_11.png';
gt_12 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_12.png';
gt_13 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_13.png';
gt_14 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_14.png';
gt_15 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_15.png';
gt_16 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_16.png';
gt_17 = basePath+'afterservice/GT/GT-ClientNeedsAnalysis_17.png';

TOPUP_1 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_1.png';
TOPUP_2 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_2.png';
TOPUP_3 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_3.png';
TOPUP_4 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_4.png';
TOPUP_5 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_5.png';
TOPUP_6 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_6.png';
TOPUP_7 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_7.png';
TOPUP_8 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_8.png';
TOPUP_9 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_9.png';
TOPUP_10 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_10.png';
TOPUP_11 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_11.png';
TOPUP_12 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_12.png';
TOPUP_13 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_13.png';
TOPUP_14 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_14.png';
TOPUP_15 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_15.png';
TOPUP_16 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_16.png';
TOPUP_17 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_17.png';
TOPUP_18 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_18.png';
TOPUP_19 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_19.png';
TOPUP_20 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_20.png';
TOPUP_21 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_21.png';
TOPUP_22 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_22.png';
TOPUP_23 = basePath+'afterservice/TOPUP/TOPUPformforHongKong_DAHK_XP_TOPUP_23.png';

FNA_1 = basePath+'afterservice/FNA/FNAform_1.png';
FNA_2 = basePath+'afterservice/FNA/FNAform_2.png';
FNA_3 = basePath+'afterservice/FNA/FNAform_3.png';
FNA_4 = basePath+'afterservice/FNA/FNAform_4.png';
FNA_5 = basePath+'afterservice/FNA/FNAform_5.png';
FNA_6 = basePath+'afterservice/FNA/FNAform_6.png';
FNA_7 = basePath+'afterservice/FNA/FNAform_7.png';

FPI_RPBWS_1 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-1.png';
FPI_RPBWS_2 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-2.png';
FPI_RPBWS_3 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-3.png';
FPI_RPBWS_4 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-4.png';
FPI_RPBWS_5 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-5.png';
FPI_RPBWS_6 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-6.png';
FPI_RPBWS_7 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-7.png';
FPI_RPBWS_8 = basePath+'afterservice/FPI-RPBWS/FPI-RPBWS-8.png';

FPI_CCCA_1 = basePath+'afterservice/FPI-CCCA/FPI-CCCA-1.png';
FPI_CCCA_2 = basePath+'afterservice/FPI-CCCA/FPI-CCCA-2.png';
FPI_CCCA_3 = basePath+'afterservice/FPI-CCCA/FPI-CCCA-3.png';
FPI_CCCA_4 = basePath+'afterservice/FPI-CCCA/FPI-CCCA-4.png';
FPI_CCCA_5 = basePath+'afterservice/FPI-CCCA/FPI-CCCA-5.png';

FPI_AOTPAP_1 = basePath+'afterservice/FPI-AOTPAP/FPI-AOTPAP-1.png';
FPI_AOTPAP_2 = basePath+'afterservice/FPI-AOTPAP/FPI-AOTPAP-2.png';
FPI_AOTPAP_3 = basePath+'afterservice/FPI-AOTPAP/FPI-AOTPAP-3.png';
FPI_AOTPAP_4 = basePath+'afterservice/FPI-AOTPAP/FPI-AOTPAP-4.png';
FPI_AOTPAP_5 = basePath+'afterservice/FPI-AOTPAP/FPI-AOTPAP-5.png';
FPI_AOTPAP_6 = basePath+'afterservice/FPI-AOTPAP/FPI-AOTPAP-6.png';

CFPI = basePath+'afterservice/Change_of_Personal_Information.png';

ChangeEmail = basePath+'afterservice/FPI_Change_Email.png';

ChangeAdd = basePath+'afterservice/FPI-Letter_for_address_amendment.png';

ChangeInv = basePath+'afterservice/FPI-change-Investors.png';

ChangeNum = basePath+'afterservice/FPI-Request_for_Mobile_Number_Change.png';

NEW_Third_1 = basePath+'afterservice/NEW_Third/NEW_Third_1.png';
NEW_Third_2 = basePath+'afterservice/NEW_Third/NEW_Third_2.png';
NEW_Third_3 = basePath+'afterservice/NEW_Third/NEW_Third_3.png';
NEW_Third_4 = basePath+'afterservice/NEW_Third/NEW_Third_4.png';

FPI_68_1 = basePath+'afterservice/FPI-68/1-FPI - Letter of change premium contribution.png';
FPI_68_2 = basePath+'afterservice/FPI-68/02a. FPI - BSO Chi & Eng.png';
FPI_68_3 = basePath+'afterservice/FPI-68/FPI - Direct Charge Authority-1.png';
FPI_68_4 = basePath+'afterservice/FPI-68/FPI - Direct Charge Authority-2.png';

FPI_70_1 = basePath+'afterservice/FPI-70/1. Top-up Instruction Template.png';
FPI_70_2 = basePath+'afterservice/FPI-70/2. Commission Disclosure Declaration-1.png';
FPI_70_3 = basePath+'afterservice/FPI-70/2. Commission Disclosure Declaration-2.png';
FPI_70_4 = basePath+'afterservice/FPI-70/3. PIC Statement-1';
FPI_70_5 = basePath+'afterservice/FPI-70/3. PIC Statement-2';
FPI_70_6 = basePath+'afterservice/FPI-70/3. PIC Statement-3';
FPI_70_7 = basePath+'afterservice/FPI-70/3. PIC Statement-4';
FPI_70_8 = basePath+'afterservice/FPI-70/3. PIC Statement-5';
FPI_70_9 = basePath+'afterservice/FPI-70/3. PIC Statement-6';

FPI_CMEA = basePath+'afterservice/FPI-Confirmation_of_Monetary_Exchange_Assignment.png';

FPI_CSA = basePath+'afterservice/FPI_Change_Servciing_Agent.png';

FPI_Self = basePath+'afterservice/FPI-Self_Certification_for_FATCA.png';

FPI_Adj = basePath+'afterservice/FPI-Adjustment.png';

FPI_CPM = basePath+'afterservice/FPI-changePaymentMode.png';

FPI_TT = basePath+'afterservice/FPI-Telegraphic-Transfer.png';

FPI_TPB = basePath+'afterservice/FPI-ThirdPaymentBank.png';

FPI_TPC = basePath+'afterservice/FPI-ThirdPaymentCredit.png';

CancelOMA_1 = basePath+'afterservice/oma_alteration-1.png';
CancelOMA_2 = basePath+'afterservice/oma_alteration-2.png';

ASS_1 = basePath+'afterservice/assignment/assignment_of_life_policy_1.png';
ASS_2 = basePath+'afterservice/assignment/assignment_of_life_policy_2.png';
ASS_3 = basePath+'afterservice/assignment/assignment_of_life_policy_3.png';
ASS_4 = basePath+'afterservice/assignment/assignment_of_life_policy_4.png';
ASS_5 = basePath+'afterservice/assignment/assignment_of_life_policy_5.png';
ASS_6 = basePath+'afterservice/assignment/assignment_of_life_policy_6.png';

BIL_1 = basePath+'afterservice/BIL/BIL_LIFE_1.png';
BIL_2 = basePath+'afterservice/BIL/BIL_LIFE_2.png';
BIL_3 = basePath+'afterservice/BIL/BIL_LIFE_3.png';
BIL_4 = basePath+'afterservice/BIL/BIL_LIFE_4.png';

FPI_Lossor_1 = basePath+'afterservice/FPI-Lossor/FPI-LossorNonReceiptPolicyDocument_1.png';
FPI_Lossor_2 = basePath+'afterservice/FPI-Lossor/FPI-LossorNonReceiptPolicyDocument_2.png';
FPI_Lossor_3 = basePath+'afterservice/FPI-Lossor/FPI-LossorNonReceiptPolicyDocument_3.png';
FPI_Lossor_4 = basePath+'afterservice/FPI-Lossor/FPI-LossorNonReceiptPolicyDocument_4.png';


FPI_OMA_1 = basePath+'afterservice/FPI-OMA/FPI-OMA_1.png';
FPI_OMA_2 = basePath+'afterservice/FPI-OMA/FPI-OMA_2.png';
FPI_OMA_3 = basePath+'afterservice/FPI-OMA/FPI-OMA_3.png';
FPI_OMA_4 = basePath+'afterservice/FPI-OMA/FPI-OMA_4.png';

FPI_Broker = basePath+'afterservice/FPI-RequestforSendPolicyEndorsementtoBroker.png';

FPI_ReName = basePath+'afterservice/FPI-RequestforNameChangeLetter.png';

GT_Madison = basePath+'afterservice/GT_Madison_ChangeRequestForm.png';

GT_FNAC = basePath+'afterservice/GT-FinancialNeedsAnalysisCover.png';

VOYA_CPI = basePath+'afterservice/Name&AddressChangeRequest_Voya_IULGlobalChoice.png';

Sign_0 = basePath+'afterservice/specimenSignature-0.png';
Sign_1 = basePath+'afterservice/specimenSignature-1.png';
Sign_2 = basePath+'afterservice/specimenSignature-2.png';
Sign_3 = basePath+'afterservice/specimenSignature-3.png';

VOYA_TO_1 = basePath+'afterservice/TransferofOwnership_Voya_IULGlobalChoice-1.png';
VOYA_TO_2 = basePath+'afterservice/TransferofOwnership_Voya_IULGlobalChoice-2.png';
VOYA_TO_3 = basePath+'afterservice/TransferofOwnership_Voya_IULGlobalChoice-3.png';

VOYA_BDV_1 = basePath+'afterservice/VOYA_BDV/BeneficiaryDesignation_Voya_IULGlobalChoice-1.png';
VOYA_BDV_2 = basePath+'afterservice/VOYA_BDV/BeneficiaryDesignation_Voya_IULGlobalChoice-2.png';
VOYA_BDV_3 = basePath+'afterservice/VOYA_BDV/BeneficiaryDesignation_Voya_IULGlobalChoice-3.png';
VOYA_BDV_4 = basePath+'afterservice/VOYA_BDV/BeneficiaryDesignation_Voya_IULGlobalChoice-4.png';
VOYA_BDV_5 = basePath+'afterservice/VOYA_BDV/BeneficiaryDesignation_Voya_IULGlobalChoice-5.png';

BiauKaiTTF = basePath+'print/BiauKai.ttf';

IFS_P_1 = basePath+'afterservice/IFS/IFS_Premier_1.png';
IFS_P_2 = basePath+'afterservice/IFS/IFS_Premier_2.png';
IFS_P_3 = basePath+'afterservice/IFS/IFS_Premier_3.png';
IFS_P_4 = basePath+'afterservice/IFS/IFS_Premier_4.png';
IFS_P_5 = basePath+'afterservice/IFS/IFS_Premier_5.png';
IFS_P_6 = basePath+'afterservice/IFS/IFS_Premier_6.png';
IFS_P_7 = basePath+'afterservice/IFS/IFS_Premier_7.png';
IFS_P_8 = basePath+'afterservice/IFS/IFS_Premier_8.png';
IFS_P_9 = basePath+'afterservice/IFS/IFS_Premier_9.png';
IFS_P_10 = basePath+'afterservice/IFS/IFS_Premier_10.png';
IFS_P_11 = basePath+'afterservice/IFS/IFS_Premier_11.png';
IFS_P_12 = basePath+'afterservice/IFS/IFS_Premier_12.png';

IFS_P_U_1 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_1.png';
IFS_P_U_2 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_2.png';
IFS_P_U_3 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_3.png';
IFS_P_U_4 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_4.png';
IFS_P_U_5 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_5.png';
IFS_P_U_6 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_6.png';
IFS_P_U_7 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_7.png';
IFS_P_U_8 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_8.png';
IFS_P_U_9 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_9.png';
IFS_P_U_10 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_10.png';
IFS_P_U_11 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_11.png';
IFS_P_U_12 = basePath+'afterservice/IFS_P_Ultra/8.IFS_Premier ultra_12.png';

update_54_1 = basePath+'afterservice/54_1_update/54_1_update_1.png';
update_54_2 = basePath+'afterservice/54_1_update/54_1_update_2.png';
update_54_3 = basePath+'afterservice/54_1_update/54_1_update_3.png';
update_54_4 = basePath+'afterservice/54_1_update/54_1_update_4.png';
update_54_5 = basePath+'afterservice/54_1_update/54_1_update_5.png';

signature_1 = basePath+'afterservice/61_1_signature/61_1_signature_1.png';
signature_2 = basePath+'afterservice/61_1_signature/61_1_signature_2.png';
signature_3 = basePath+'afterservice/61_1_signature/61_1_signature_3.png';
signature_4 = basePath+'afterservice/61_1_signature/61_1_signature_4.png';
signature_5 = basePath+'afterservice/61_1_signature/61_1_signature_5.png';
signature_6 = basePath+'afterservice/61_1_signature/61_1_signature_6.png';

policy_92_1 = basePath+'afterservice/92_policy/92_policy_1.png';
policy_92_2 = basePath+'afterservice/92_policy/92_policy_2.png';
policy_92_3 = basePath+'afterservice/92_policy/92_policy_3.png';
policy_92_4 = basePath+'afterservice/92_policy/92_policy_4.png';
policy_92_5 = basePath+'afterservice/92_policy/92_policy_5.png';
policy_92_6 = basePath+'afterservice/92_policy/92_policy_6.png';
policy_92_7 = basePath+'afterservice/92_policy/92_policy_7.png';
policy_92_8 = basePath+'afterservice/92_policy/92_policy_8.png';
policy_92_9 = basePath+'afterservice/92_policy/92_policy_9.png';
policy_92_10 = basePath+'afterservice/92_policy/92_policy_10.png';
policy_92_11 = basePath+'afterservice/92_policy/92_policy_11.png';
policy_92_12 = basePath+'afterservice/92_policy/92_policy_12.png';
policy_92_13 = basePath+'afterservice/92_policy/92_policy_13.png';

// BiauKaiTTF = basePath+'print/edukai-3.ttf'; // 掉字
// BiauKaiTTF = basePath+'print/BiauKai2.ttf'; // 多字時還是不行
// BiauKaiTTF = basePath+'print/BiauKai3.ttf'; // 完全不行
// BiauKaiTTF = basePath+'print/BiauKai4.ttf'; // 不是unicode的
// BiauKaiTTF = basePath+'print/msyh.ttf'; // 雅黑 完全正常
// BiauKaiTTF = basePath+'print/msjh.ttf'; // 正體 完全正常
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; // No unicode cmap
// BiauKaiTTF = basePath+'print/kaiu.ttf'; // no
// BiauKaiTTF = basePath+'print/kaiu_1.ttf'; // no
// BiauKaiTTF = basePath+'print/kai-pc.ttf'; // no
// BiauKaiTTF = basePath+'print/ukai_1.ttf'; // some
// BiauKaiTTF = basePath+'print/ukai_2.ttf'; // no
//BiauKaiTTF = basePath+'print/ukai_3.ttf'; // no
// BiauKaiTTF = basePath+'print/ukai_0.ttf'; // no
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; //
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; //
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; //
// BiauKaiTTF = basePath+'print/Kaiti_1.ttf'; //

msjhTTF = basePath+'print/msjh.ttf'; // ok 正體
msyhTTF = basePath+'print/msyh.ttf'; // ok 雅黑


//A4: [595.28, 841.89],
a4pageHeight = 841.89;
a4pageWidth = 595.28;
a4pageWidthHalf = a4pageWidth/2;
a4pageWidthHalf2 = a4pageWidth/4;
