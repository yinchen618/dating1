import { Email } from 'meteor/email'
import { check } from 'meteor/check'

var world = function() {
    var now = new Date();

    // console.log('Check Send Mail Box! ' + now);

    var e = Emails.find({sent_status_id: "2", sent_time:{$lte: now}}).fetch();
    if(e.length){
        var from = "notice@hanburyifa.com";
        for(var i=0; i<e.length; i++){
        	var entry = e[i];

            // if(!entry.receiver || typeof entry.receiver == "undefined"){
            //     continue;
            // }

            // console.log("entry");
            // console.log(entry);

        	var id = entry._id;
          	delete entry._id;

            var to = entry.receiver;
            var cc = entry.cc;
            var bcc = entry.bcc;
            var subject = entry.subject;
            var html = entry.context;

            // console.log(to);
            // console.log(subject);

            var attachments = entry.attachments;
            // this.unblock();

            var obj = {};
            obj = {
              to: to,
              cc: cc,
              bcc: bcc,
              from: from,
              subject: subject,
              html: html,
              attachments: attachments
            };

            Email.send(obj);
            entry.sent_status_id = "1";
            Emails.update(id, {$set: entry });
        }


        // console.log("排程寄信：");
        // console.log(e);
    }
}

// var myBirthDay = function() {
//     // console.log('My Birth Day!');
// }

var cron = new Meteor.Cron({
    events: {
        "* * * * *": world,
        // "0 0 18 6 *": myBirthDay
    }
});
