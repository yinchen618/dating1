
Clients.after.insert(function (userId, doc) {
    // console.log("Clients.after.insert");
    // console.log(doc);
    var obj = {};
    obj.client_id = doc._id;
    obj.client_uid = doc.uid;
    obj.name_cht = doc.name_cht || "";
    obj.name_eng = doc.name_eng || "";
    obj.email = doc.email || "";
    obj.agent_id = doc.agent_id || "";
    obj.agent_text = doc.agent_text || "";
    obj.fake = 1; //?
    obj.insertedAt = new Date();

    // Clients.insert(obj);
    if(!!doc.birthday){
        let arr = doc.birthday.split("/");
        let yyyy = arr[0];
        let mm = arr[1];
        let dd = arr[2];
        Clients.update(doc, {$set:{
            "birthday_yyyy" : Number(yyyy),
            "birthday_mm" : Number(mm),
            "birthday_dd" : Number(dd)
        }});
    }
});


Clients.after.update(function (userId, doc, fieldNames, modifier, options) {
    var obj = {};
    // delete obj._id;

/*    "name_cht" : "王連銀渾",
    "name_eng" : "Wang Lien,Yin-Hun",
    "sexual_text" : "女",
    "sexual_id" : "2",
    "title_text" : "",
    "birthday" : "1949/12/13",
    "identify" : "N200155746",
    "passport" : "210275560",
    "marriage_id" : "",
    "marriage_text" : "",
    "cellnum" : "0922-747-203",
    "faxnum" : "",
    "country_text" : "台灣",
    "addr1_phone" : "(02) 2937-0726",
    "addr1_post5" : "116",
    "addr1_cht" : "台北市文山區秀明路2段5號12樓",
    "addr1_eng" : "",
    "addr2_phone" : "",
    "addr2_post5" : "",
    "addr2_cht" : "",
    "addr2_eng" : "",
    "email" : "tonywang512@gmail.com",
    "fin_type" : "",
    "fin_company_cht" : "",
    "fin_company_eng" : "",
    "fin_addr_cht" : "",
    "fin_addr_eng" : "",
    "fin_yearsalary" : "",
    "fin_invexp_text" : "",
    "fin_purpose_text" : "",
    "ps" : "系統匯入",
    "agent_id" : "NR9A9xsTj55iN6su7",
    "agent_text" : "Aaron",*/
    obj.name_cht = doc.name_cht || "";
    obj.name_eng = doc.name_eng || "";
    // obj.birthday = doc.birthday;
    // obj.identify = doc.identify;
    // obj.passport = doc.passport;
    obj.email = doc.email || "";
    obj.agent_id = doc.agent_id || "";
    obj.agent_text = doc.agent_text || "";
    obj.contactnum = doc.cellnum || "";

    // console.log("doc");
    // console.log(doc);
    // console.log("obj");
    // console.log(obj);

    Portfolios.update({client_id: doc._id}, {$set: obj}, {multi: true});
});
Clients.before.update(function (userId, doc, fieldNames, modifier, options) {
    modifier.$set.updatedAt = new Date();

    if(!!doc.birthday){
        let arr = doc.birthday.split("/");
        let yyyy = arr[0];
        let mm = arr[1];
        let dd = arr[2];

        modifier.$set.birthday_yyyy = Number(yyyy);
        modifier.$set.birthday_mm = Number(mm);
        modifier.$set.birthday_dd = Number(dd);
    }
});
Clients.before.remove(function (userId, doc) {
    // console.log("Clients.before.remove doc");
    // console.log(doc);
    Portfolios.remove({client_id: doc._id});
});
Portfolios.after.insert(function (userId, doc) {
    // console.log("Clients.after.insert");
    // console.log(doc);
    var obj = {};
  /*  obj.client_id = doc._id;
    obj.client_uid = doc.uid;
    obj.name_cht = doc.name_cht || "";
    obj.name_eng = doc.name_eng || "";
    obj.email = doc.email || "";
    obj.agent_id = doc.agent_id || "";
    obj.agent_text = doc.agent_text || "";
    obj.fake = 1; //?*/
    // obj.effective_date = new Date();

    // Portfolios.insert(obj);
    //
    obj.portfolios_id = doc._id;
    obj.template_id = doc.template_id;
    var w = Workdays.findOne({type_id: doc.template_id}, {sort:{ order_id: 1}});
    obj.status_id = w._id;
    obj.status_text = w.type_name;
    obj.work_days = w.work_days;
    obj.insertedAt = new Date();
    obj.isFinish = "0";
    PortfolioStatus.insert(obj);

    // if(!!doc.effective_date){
    //     let ins = currentdate_eff(doc.effective_date)
    //     let arr = ins.split("/");
    //     let yyyy = arr[0];
    //     let mm = arr[1];
    //     let dd = arr[2];
    //     Portfolios.update(doc, {$set:{
    //         "effective_date_yyyy" : Number(yyyy),
    //         "effective_date_mm" : Number(mm),
    //         "effective_date_dd" : Number(dd)
    //     }});
    //     console.log("Portfolios");
    // }

});
Portfolios.before.insert(function (userId, doc) {
    // console.log("Portfolios.before.insert");
    // console.log(doc);

    // 如果是真的要建的話，就把之前fake=1的檔案給刪掉
    if(!doc.fake){
        Portfolios.remove({client_id: doc.client_id, fake: 1});
    }

    doc.insertedAt = new Date();
    // Portfolios.insert(obj);
});
// Portfolios.before.update(function (userId, doc, fieldNames, modifier, options) {
//     if(modifier.$set) modifier.$set.updatedAt = new Date();

//     if(!!doc.effective_date){
//         let ins = currentdate_eff(doc.effective_date)
//         let arr = ins.split("/");
//         let yyyy = arr[0];
//         let mm = arr[1];
//         let dd = arr[2];

//         modifier.$set.effective_date_yyyy = Number(yyyy);
//         modifier.$set.effective_date_mm = Number(mm);
//         modifier.$set.effective_date_dd = Number(dd);
//     }
// });
/*Portfolios.before.update(function (userId, doc, fieldNames, modifier, options) {
    // modifier.$set.updatedAt = new Date();

    // if(!!doc.birthday){
    //     let arr = doc.birthday.split("/");
    //     let yyyy = arr[0];
    //     let mm = arr[1];
    //     let dd = arr[2];

    //     modifier.$set.birthday_yyyy = Number(yyyy);
    //     modifier.$set.birthday_mm = Number(mm);
    //     modifier.$set.birthday_dd = Number(dd);
    // }
    modifier.$set.current_Status = doc.next_Status;
    modifier.$set.current_status_days = doc.next_status_days;
});*/
Portfolios.after.update(function (userId, doc, fieldNames, modifier, options) {
    // console.log("doc");
    // console.log(doc);
    // console.log("modifier");
    // console.log(modifier);
    if(doc.no_updated_hook == 1 || !doc.current_Status){
        return;
    }
    if(!PortfolioStatus.findOne({portfolios_id: doc._id, isFinish: "0"})){
        var obj = {};
        obj.portfolios_id = doc._id;
        obj.template_id = doc.template_id;
        var w = Workdays.findOne({_id: doc.current_Status}, {sort:{ order_id: 1}});
        obj.workdays_id = w._id;
        obj.status_text = w.status;
        obj.type_text = w.type_name;
        obj.work_days = w.work_days;
        obj.insertedAt = new Date();
        obj.isFinish = "0";
        PortfolioStatus.insert(obj);
    }
    var ps = PortfolioStatus.findOne({portfolios_id: doc._id, isFinish: "0"});
    var d = new Date();
    var timediff = (d.getTime() - new Date(ps.insertedAt).getTime())/1000;
    PortfolioStatus.update({_id: ps._id}, {$set:{
        isFinish: "1",
        closedAt: d,
        timediff: timediff
    }});

    var obj = {};
    obj.portfolios_id = doc._id;
    obj.template_id = doc.template_id;
    var w = Workdays.findOne({_id: doc.current_Status});
    // console.log(w);
    // obj.workdays_id = w._id;
    obj.status_text = w._id;
    obj.status_text = w.status;
    obj.type_text = w.type_name;
    obj.work_days = w.work_days;
    obj.insertedAt = new Date();
    obj.isFinish = "0";
    PortfolioStatus.insert(obj);
});
Afterservice.before.insert(function (userId, doc) {
    doc.insertedAt = new Date();
    // Portfolios.insert(obj);
});
Afterservice.before.update(function (userId, doc, fieldNames, modifier, options) {
    modifier.$set.updatedAt = new Date();
});

Booking.after.update(function (userId, doc, fieldNames, modifier, options) {

    let n = 0;
    let arrbook = Booking.find({year: doc.year,month: doc.month, bg_id: doc.bg_id }).fetch();
    for (let i = 0; i < arrbook.length; i++) {
        if(!isNaN(arrbook[i].total)){
            n += arrbook[i].total;
        }
    }

    let book_bg    = arrbook[0].bg_id;
    let book_year  = arrbook[0].year;
    let book_month = arrbook[0].month;

    // n = commaSeparateNumber(n);
    switch(book_month){
      case "1" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month1_total": n }}); break;
      case "2" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month2_total": n }}); break;
      case "3" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month3_total": n }}); break;
      case "4" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month4_total": n }}); break;
      case "5" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month5_total": n }}); break;
      case "6" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month6_total": n }}); break;
      case "7" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month7_total": n }}); break;
      case "8" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month8_total": n }}); break;
      case "9" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month9_total": n }}); break;
      case "10":Accountyear.update({ bg_id: book_bg, value: book_year }, {$set:{ "month10_total": n }}); break;
      case "11":Accountyear.update({ bg_id: book_bg, value: book_year }, {$set:{ "month11_total": n }}); break;
      case "12":Accountyear.update({ bg_id: book_bg, value: book_year }, {$set:{ "month12_total": n }}); break;
      default : break;
    }
});
Booking.after.remove(function (userId, doc) {

    let n = 0;
    let arrbook = Booking.find({year: doc.year,month: doc.month, bg_id: doc.bg_id }).fetch();
    for (let i = 0; i < arrbook.length; i++) {
        console.log("arrbook[i].total: "+arrbook[i].total);
        if(!isNaN(arrbook[i].total)){
            n += arrbook[i].total;
        }
    }

    let book_bg    = arrbook[0].bg_id;
    let book_year  = arrbook[0].year;
    let book_month = arrbook[0].month;

    n = commaSeparateNumber(n);
    switch(book_month){
      case "1" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month1_total": n }}); break;
      case "2" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month2_total": n }}); break;
      case "3" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month3_total": n }}); break;
      case "4" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month4_total": n }}); break;
      case "5" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month5_total": n }}); break;
      case "6" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month6_total": n }}); break;
      case "7" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month7_total": n }}); break;
      case "8" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month8_total": n }}); break;
      case "9" :Accountyear.update({ bg_id: book_bg, value: book_year}, {$set:{ "month9_total": n }}); break;
      case "10":Accountyear.update({ bg_id: book_bg, value: book_year }, {$set:{ "month10_total": n }}); break;
      case "11":Accountyear.update({ bg_id: book_bg, value: book_year }, {$set:{ "month11_total": n }}); break;
      case "12":Accountyear.update({ bg_id: book_bg, value: book_year }, {$set:{ "month12_total": n }}); break;
      default : break;
    }
});
Uploads.before.insert(function (userId, doc) {
	// console.log("Uploads.before.insert");
	if(!doc.name && typeof doc.name != "string"){
		return;
	}
	var arr = doc.name.split("_");
	for(var i=0; i<arr.length; i++){
		var n = arr[i];
		// if(typeof n != "string"){
		// 	continue;
		// }
		// console.log("n: "+ n);
		// console.log(Portfolios.find({account_num: n}).count());
		if(Portfolios.find({account_num: n}).count() > 0){
			var p = Portfolios.findOne({account_num: n});
		 	doc.receiver_name = p.name_cht;
		 	doc.receiver_email = p.email;

		 	break;
		}
	}
});