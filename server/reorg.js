// if(1){
//     console.log("building birthday in Clients");
//     Clients.find().forEach(function(u){
//         if(!!u.birthday){
//             let arr = u.birthday.split("/");
//             let yyyy = arr[0];
//             let mm = arr[1];
//             let dd = arr[2];
//             Clients.update(u, {$set:{
//                 "birthday_yyyy" : Number(yyyy),
//                 "birthday_mm" : Number(mm),
//                 "birthday_dd" : Number(dd)
//             }});
//             // console.log("client "+ u.name_cht +" birthday: "+ u.birthday + " to: "+ yyyy + " "+ mm + " "+ dd );

//         }
//     });
// }

// if(1){
//     console.log("building birthday in Portfolios");
//     Portfolios.find().forEach(function(u){
//         if(!!u.effective_date){
//             let ins = currentdate_eff(u.effective_date)
//             let arr = ins.split("/");
//             let yyyy = arr[0];
//             let mm = arr[1];
//             let dd = arr[2];
//             Portfolios.update(u, {$set:{
//                 "effective_date_yyyy" : Number(yyyy),
//                 "effective_date_mm" : Number(mm),
//                 "effective_date_dd" : Number(dd)
//             }});
//             console.log("client "+ u.name_cht +" effective_date: "+ u.effective_date + " to: "+ yyyy + " "+ mm + " "+ dd );

//         }
//     });
// }
if( Emailauto.find().count() == 0 ){
	Emailauto.insert({ type: "1",  cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "客戶生日提醒", context:"Dear [agent], <BR><BR>以下檢附為近期的客戶生日名單，請知悉。<BR>如有任何交辦事項，再煩請通知營運部同仁協助處理，謝謝您。<BR><BR>" });
	Emailauto.insert({ type: "2",  cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "客戶繳費提醒", context:"Dear [agent], <BR><BR>以下檢附為近期的VOYA保費應繳名單，請知悉。<BR>如有任何交辦事項，再煩請通知營運部同仁協助處理，謝謝您。<BR><BR>" });
	Emailauto.insert({ type: "3a", cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "[請假審核] 代理人審核", context:"申請人：[apply_name]<BR><BR>申請假別 [dayoff_type]，<BR>於 [start_time] ~ [end_time]，共 [dayoff_hours]。<BR><BR>請至以下差勤系統簽核查收，謝謝您。<BR><BR>[url]" });
	Emailauto.insert({ type: "3b", cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "[請假審核] 主管審核", context:"申請人：[apply_name]<BR><BR>申請假別 [dayoff_type]，<BR>於 [start_time] ~ [end_time]，共 [dayoff_hours]。<BR><BR>請至以下差勤系統簽核查收，謝謝您。<BR><BR>[url]" });
	Emailauto.insert({ type: "3c", cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "[請假審核] 人資審核", context:"申請人：[apply_name]<BR><BR>申請假別 [dayoff_type]，<BR>於 [start_time] ~ [end_time]，共 [dayoff_hours]。<BR><BR>請至以下差勤系統簽核查收，謝謝您。<BR><BR>[url]" });
	Emailauto.insert({ type: "3d", cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "[請假成功] 申請通過", context:"申請人：[apply_name]<BR><BR>申請假別 [dayoff_type]，<BR>於 [start_time] ~ [end_time]，共 [dayoff_hours]。<BR><BR>已經由主管及人資主管簽核批准，謝謝您。" });
	Emailauto.insert({ type: "4",  cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "客戶新件進度", context:"Dear [agent], <BR><BR>以下檢附為近期的新件進度名單，請知悉。<BR>如有任何交辦事項，再煩請通知營運部同仁協助處理，謝謝您。<BR><BR>" });
	Emailauto.insert({ type: "5",  cc:"ac.chuang@hanburyifa.com,thomas.yang@hanburyifa.com", bcc:"", subject: "客戶案件追蹤", context:"Dear [agent], <BR><BR>以下檢附為近期的案件追蹤名單，請知悉。<BR>如有任何交辦事項，再煩請通知營運部同仁協助處理，謝謝您。<BR><BR>" });
}
