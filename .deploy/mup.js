module.exports = {
  servers: {
    one: {
      // host: "ec2-52-197-160-117.ap-northeast-1.compute.amazonaws.com",
      // username: 'ubuntu',
      // pem: "/Users/michaelchen/Dropbox/_code/lb0505.pem"
      host: "13.75.111.29",
      username: 'infowintech',
      password: 'Info24934467'
    }
  },

  meteor: {
    name: 'hanbury2',
    path: "/Users/michaelchen/Dropbox/_code/hanbury2",
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
      debug: true,
      // debug: false,
    },
    env: {
      PORT: '8000',
      // ROOT_URL: "http://ec2-52-197-160-117.ap-northeast-1.compute.amazonaws.com",
      ROOT_URL: "http://13.75.111.29",
      MONGO_URL: 'mongodb://localhost:27017/meteor'
    },
    dockerImage: 'abernix/meteord:base',
    enableUploadProgressBar: true,
    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },

  mongo: {
    version: '3.4.0',
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};
