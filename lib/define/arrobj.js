Array.prototype.clear = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

Array.prototype.clean = function() {
  var deleteValue = "";
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue || this[i] === null || this[i] === undefined ) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};
Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};


funcObjDelNullProp = function (test){
    for (var i in test) {
      if (test[i] === null || test[i] === undefined || test[i] == "") {
      // test[i] === undefined is probably not very useful here
        delete test[i];
      }
    }
}

funcObjFind = function (obj, id){
    var ret = "";
    obj.forEach(function(entry) {
        // console.log(entry);
        if(Number(entry.id) == id){
            ret = entry.value;
        }
    });
    return ret;
}
funcObjFind2 = function (obj, id){
    var ret = "";
    obj.forEach(function(entry) {
        // console.log(entry);
        if(Number(entry.value) == id){
            ret = entry.text;
        }
    });
    return ret;
}


// 檢查這個object是不是空的
// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

isEmpty = function(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}
