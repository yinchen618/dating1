// Convert numbers to words
// copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
// permission to use this Javascript on your web page is granted
// provided that all of the code (including this copyright notice) is
// used exactly as shown (you can change the numbering system if you wish)

// American Numbering System
// var th = ['','thousand','million', 'billion','trillion'];
// // uncomment this line for English Number System
// // var th = ['','thousand','million', 'milliard','billion'];

// var dg = ['zero','one','two','three','four',
// 'five','six','seven','eight','nine']; var tn =
// ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen',
// 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty',
// 'sixty','seventy','eighty','ninety']; function toWords(s){s = s.toString(); s =
// s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x =
// s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n =
// s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if
// ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;}
// else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str +=
// dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk)
// str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str +=
// 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return
// str.replace(/\s+/g,' ');}
//數字轉英文
numToWords = function(number) {
    //Validates the number input and makes it a string
	    // while (number.search(",") >= 0) {
	    //     number = (number + "").replace(',', '');
	    // }

    if (typeof number === 'string') {
    	number = number.replace(/\,/g,'');
        number = parseInt(number, 10);
    }
    if (typeof number === 'number' && isFinite(number)) {
        number = number.toString(10);
    } else {
        return 'This is not a valid number';
    }

    //Creates an array with the number's digits and
    //adds the necessary amount of 0 to make it fully
    //divisible by 3
    var digits = number.split('');
    while (digits.length % 3 !== 0) {
        digits.unshift('0');
    }


    //Groups the digits in groups of three
    var digitsGroup = [];
    var numberOfGroups = digits.length / 3;
    for (var i = 0; i < numberOfGroups; i++) {
        digitsGroup[i] = digits.splice(0, 3);
    }
    console.log(digitsGroup); //debug

    //Change the group's numerical values to text
    var digitsGroupLen = digitsGroup.length;
    var numTxt = [
        [null, 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'], //hundreds
        [null, 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'], //tens
        [null, 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'] //ones
        ];
    var tenthsDifferent = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

    // j maps the groups in the digitsGroup
    // k maps the element's position in the group to the numTxt equivalent
    // k values: 0 = hundreds, 1 = tens, 2 = ones
    for (var j = 0; j < digitsGroupLen; j++) {
        for (var k = 0; k < 3; k++) {
            var currentValue = digitsGroup[j][k];
            digitsGroup[j][k] = numTxt[k][currentValue];
            if (k === 0 && currentValue !== '0') { // !==0 avoids creating a string "null hundred"
                digitsGroup[j][k] += ' hundred ';
            } else if (k === 1 && currentValue === '1') { //Changes the value in the tens place and erases the value in the ones place
                digitsGroup[j][k] = tenthsDifferent[digitsGroup[j][2]];
                digitsGroup[j][2] = 0; //Sets to null. Because it sets the next k to be evaluated, setting this to null doesn't work.
            }
        }
    }

    console.log(digitsGroup); //debug

    //Adds '-' for gramar, cleans all null values, joins the group's elements into a string
    for (var l = 0; l < digitsGroupLen; l++) {
        if (digitsGroup[l][1] && digitsGroup[l][2]) {
            digitsGroup[l][1] += '-';
        }
        digitsGroup[l].filter(function (e) {return e !== null});
        digitsGroup[l] = digitsGroup[l].join('');
    }

    console.log(digitsGroup); //debug

    //Adds thousand, millions, billion and etc to the respective string.
    var posfix = [null, 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion'];
    if (digitsGroupLen > 1) {
        var posfixRange = posfix.splice(0, digitsGroupLen).reverse();
        for (var m = 0; m < digitsGroupLen - 1; m++) { //'-1' prevents adding a null posfix to the last group
            if (digitsGroup[m]) {
                digitsGroup[m] += ' ' + posfixRange[m];
            }
        }
    }

    console.log(digitsGroup); //debug

    //Joins all the string into one and returns it
    return (digitsGroup.join(" ")).toUpperCase();

} //

// String.prototype.nl2br = function (str, is_xhtml) {
//     var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
//     return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
// }

//number加千分號
commaSeparateNumber = function(val){
  if(!val) return "";
  val = val.toString().replace(/,/g, "");
  // return new Intl.NumberFormat().format(Number(val.toString()));
  // return Number(parseFloat(val).toFixed(2)).toLocaleString('en');
  return Number(parseFloat(val)).toLocaleString('en');
  // val = val.toString().replace(/\D/g, '').replace(/,/g, "");
  // val = Number(val);
  // while (/(\d+)(\d{3})/.test(val.toString())){
  //   val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
  // }
  // return val;
}

//移除千分號
commaToNumber = function(val){
  if(!val) return "";
  val = val.toString().replace(/(\d+),(?=\d{3}(\D|$))/g, "$1");
  // return Number(parseFloat(val));
  return parseFloat(val);

  // return new Intl.NumberFormat().format(Number(val.toString()));
  // return Number(parseFloat(val).toFixed(2)).toLocaleString('en');
  // val = Number(val);
  // while (/(\d+)(\d{3})/.test(val.toString())){
  //   val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
  // }
  // val = val.toString().replace(/\D/g, '').replace(/,/g, "");
  // return parseFloat(val);
}

funcStrPad = function(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}


String.prototype.Blength = function() {
    var arr = this.match(/[^\x00-\xff]/ig);
    return  arr == null ? this.length : this.length + arr.length;
}

String.prototype.mydate = function() {
	var dd = new Date(this);
	return dd.getFullYear()+'-' + (dd.getMonth()+1) + '-'+dd.getDate() +" "+dd.getHours() +':'+ dd.getMinutes() +':'+ dd.getSeconds();
}

String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};

String.prototype.cutStrToArr = function(cutNum){
	var arrStr = [];
	var start = next = 0;

	if(this.length < cutNum){ // 不用分割
		arrStr.push(this.toString());
	}
	else{
		// console.log("ori: " + this);
		cutNum = cutNum - 3;
		while(start < this.length && next != -1){

			next = this.indexOf(" ", start+cutNum);
			if(next != -1){
				arrStr.push(this.substring(start, next).trim());
				start = next;
			}
			else{
				arrStr.push(this.substring(start, this.length).trim());
				// console.log("start: "+ start + " next: "+ next );
			}
		}
	}
	// 每列的名字齊頭
	// console.log("arrStr");
	// console.log(arrStr);

	if(arrStr.length == 1){
	    return arrStr;
	}
	else if(arrStr.length > 1){
		// 先弄成名字的二維陣列
		var arrAll = [];
		for(i=0; i<arrStr.length; i++){
			arrAll.push(arrStr[i].split(" "));
		}
		// console.log("");
		// console.log("arrAll");
		// console.log(arrAll);
		// console.log(arrStr);

		var maxArr = 0;
		for(i=0; i<arrAll.length; i++){
			if(arrAll[i].length > maxArr) maxArr = arrAll[i].length;
		}
		var arrMaxNum = [];
		for(i=0; i<maxArr; i++){
			arrMaxNum.push(0);
		}
		for(i=0; i<arrAll.length; i++){
			for(j=0; j<arrAll[i].length; j++){
				if(!!arrAll[i][j] && arrAll[i][j].length > arrMaxNum[j]) arrMaxNum[j] = arrAll[i][j].length;
			}
		}
		// console.log("arrMaxNum");
		// console.log(arrMaxNum);

		for(i=0; i<arrAll.length; i++){
			for(j=0; j<arrAll[i].length; j++){
				if(!!arrAll[i][j] && arrAll[i][j].length < arrMaxNum[j]){
					var sp = "";
					for(k=0; k< arrMaxNum[j]-arrAll[i][j].length; k++){
						sp += " ";
					}
					arrAll[i][j] += sp;
				}
			}
		}
		// console.log("arrAll");
		// console.log(arrAll);

		var arrAll2 = [];
		for(i=0; i<arrAll.length; i++){
			arrAll2.push(arrAll[i].join(" "));
		}
		// console.log("arrAll2");
		// console.log(arrAll2);

    	return arrAll2;
	}
}

String.prototype.commaToSpace = function(){
    return this.replace(/,/g, " ");
}

String.prototype.toStraight = function(){
    var hex, i;

    var result = "";
    for (i=0; i<this.length; i++) {
        hex = this.charAt(i) + "\n";
        result += hex;
    }
    return result
}

// prayAlayout = {};
String.prototype.hexEncode = function(){
    var hex, i;

    var result = "";
    for (i=0; i<this.length; i++) {
        hex = this.charCodeAt(i).toString(16);
        result += ("000"+hex).slice(-4);
    }

    return result
}

String.prototype.hexDecode = function(){
    var j;
    var hexes = this.match(/.{1,4}/g) || [];
    var back = "";
    for(j = 0; j<hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }

    return back;
}

funcPrayAlayout = function (obj){

	// console.log(obj.data);
	var arrObj = obj.data;

	function pageData(str_top, str_left, str_center, str_right){
	// function pageData(){
		return [
			{
				columns:[
	                {
	                  width: 140,
	                  text: ''
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                },
	                {
	                  width: 200,
	 			      fontSize: 18,
	 			      alignment:"center",
	                  // text: 'hea總燈主der here 123'
	                  text: str_top
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                }
	            ],
	            margin: [0, 0, 0, 20]
			},
			{
				columns:[
	                {
	                  width: 132,
	                  text: ''
	                },
	                {
	        	        image: prayA_top,
	        	        width: 250
	        		}
	            ]
			},
	    	{
	    	    columns:[
	                { // total width: 160
	                  width: 100,
	                  text: ''
	                },
	                {
	                	width: 60,
						alignment:"right",
						margin: [0, 0, 5, 0],
	 			    	fontSize: 20,
	                 	text: str_left
	                },
	        	    {
	        	        image: prayA_middle,
	        	        width: 20,
	        	        height: 460
	        		},
	                {
						width: 158,
						alignment:"center",
						fontSize: 30,
						margin: [5, 20, 5, 20],
						text: str_center
	                },
	        	    {
	        	        image: prayA_middle,
	        	        width: 20,
	        	        height: 460
	        		},
	                {
						width: 24,
						fontSize: 20,
						margin: [5, 0, 0, 0],
						text: str_right
	                }
	    	    ]
	    	},
	    	{
	    	    columns:[
	                {
	                  width: 132,
	                  text: ''
	                },
	        	    {
	        	        image: prayA_bottom,
	        	        width: 250
	        		}
	    	    ]
	    	},
			{
				columns:[
	                {
	                  width: 140,
	                  text: ''
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                },
	                {
	                  width: 200,
	 			      // fontSize: 18,
	 			      // alignment:"center",
	                  text: ''
	                },
	                {
	                  width: 20,
	 			      fontSize: 18,
	                  text: '|'
	                }
	            ],
	            margin: [0, 30, 0, 0]
			}
		];
	}

	var content = [];

	arrObj.forEach(function(entry) {
		if(!!entry.type_text && !!entry.prayitem_text && !!entry.prayserial && !!entry.livename_text){

			var top = entry.prayitem_text + " " + entry.prayserial;
			if(entry.type_text.indexOf("拔") >= 0){
				var left = entry.livename_text;
				var center = entry.passname_text;
			}
			else{
				var left = "";
				var center = entry.livename_text;
			}
			var right = entry.addr || "";

			content.push(pageData(top, left, center, right));
		}
	});
	return content;
}
