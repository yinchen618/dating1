
// http://liaosankai.pixnet.net/blog/post/24165900-%E8%BA%AB%E4%BB%BD%E8%AD%89%E9%A9%97%E8%AD%89%E7%A8%8B%E5%BC%8F-for-javascript-(%E7%B2%BE%E7%B0%A1%E7%89%88)
//***************************
// 台灣身份證產生簡短版 javascript 版
//***************************
getTwID = function(value){
    //建立字母分數陣列(A~Z)
    var city = new Array(
         1,10,19,28,37,46,55,64,39,73,82, 2,11,
        20,48,29,38,47,56,65,74,83,21, 3,12,30
    )
    //建立隨機身份證碼
    var id = new Array();

    if(value.length != 10){
        return false;
    }

    for (var i = 0; i < value.length-1; i++) {
        id[i] = value.charAt(i);
    };

    // id[0] = String.fromCharCode(Math.floor(Math.random() * (26)) + 65);
    // id[1] = Math.floor(Math.random() * (2)) + 1;
    // for(var i=2; i<9; i++){
    //     id[i] = Math.floor(Math.random() * (9)) + 0;
    // }


    //計算總分
    var total = city[id[0].charCodeAt(0)-65];
    for(var i=1; i<=8; i++){
        total += eval(id[i]) * (9 - i);
    }
    //計算最尾碼
    var total_arr = (total+'').split('');
    var lastChar = eval(10-total_arr[total_arr.length-1]);
    var lastChar_arr = (lastChar+'').split('');
    //補上最後檢查碼
    id[id.length++] = lastChar_arr[lastChar_arr.length-1];

    if(id[9] == value[9]){
        return true;
    }
    return false;
    //回傳結果
    // return id.join('');
}
jobyear = function(d) {
    if(!d){
        return "";
    }
    var JobDays = (new Date - new Date(d))/1000/3600/24; //365.2422
    var year = parseInt( JobDays / 365 );
    var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
    if(year >= 1){
        return year.toString() + "年" + day + "天"; // 幾個365天 就幾年
    }
    if( day > 182){
        return day+"天(未滿一年)";
    }
    else{
        return day+"天(未滿半年)";
    }
}
// jobyear = function(d) {
//     if(!d){
//         return "";
//     }
//     var JobDays = (new Date - new Date(d))/1000/3600/24; //365.2422
//     var year = parseInt( JobDays / 365 );
//     if(year >= 1){
//         return year.toString() + "年"; // 幾個365天 就幾年
//     }
//     var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
//     if( day > 182){
//         return day+"天(未滿一年)";
//     }
//     else{
//         return day+"天(未滿半年)";
//     }
// }

ThisYearDayoff = function(d) {
    if(!d){
        return "";
    }
    var JobDays = (new Date() - new Date(d))/1000/3600/24; //365.2422
    var year = parseInt( JobDays / 365 );
    var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
        // return year.toString() + "年"; // 幾個365天 就幾年
    if(year < 1 ){
        if(day < 183){
            return 0;
        }
        return 3;
    }
    else if(year >= 1 && year < 2){
        return 7;
    }
    else if(year >= 2 && year < 3){
        return 10;
    }
    else if(year >= 3 && year < 5){
        return 14;
    }
    else if(year >= 5 && year < 10){
        return 15;
    }
    else if(year >= 10){
        var dayoff = (year - 10) + 16;
        if(dayoff > 30){
            dayoff = 30;
        }
        return dayoff;
    }
}

LastYearDayoff = function(d) {
    if(!d){
        return "";
    }
    var JobDays = (new Date().addDays(-365) - new Date(d))/1000/3600/24; //365.2422
    var year = parseInt( JobDays / 365 );
    var day = parseInt(JobDays % 365 ); // 半年用182.5天去算
        // return year.toString() + "年"; // 幾個365天 就幾年
    if(year < 1 ){
        if(day < 183){
            return 0;
        }
        return 3;
    }
    else if(year >= 1 && year < 2){
        return 7;
    }
    else if(year >= 2 && year < 3){
        return 10;
    }
    else if(year >= 3 && year < 5){
        return 14;
    }
    else if(year >= 5 && year < 10){
        return 15;
    }
    else if(year >= 10){
        var dayoff = (year - 10) + 16;
        if(dayoff > 30){
            dayoff = 30;
        }
        return dayoff;
    }
}

JSONToCSVConvertor = function(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
// exportTableToCSV = function($table, filename) {
//     var $headers = $table.find('tr:has(th)'),
//         $rows = $table.find('tr:has(td)')


//         // Temporary delimiter characters unlikely to be typed by keyboard
//         // This is to avoid accidentally splitting the actual contents
//         tmpColDelim = String.fromCharCode(11), // vertical tab character
//         tmpRowDelim = String.fromCharCode(0), // null character

//         // actual delimiter characters for CSV format
//         colDelim = '","',
//         rowDelim = '"\r\n"',

//         // Grab text from table into CSV formatted string
//         csv = '"' + $rows.map(function (i, row) {
//             var $row = $(row),
//                 $cols = $row.find('td');
//                 if(!$cols.length) $cols = $row.find('th');
//             return $cols.map(function (j, col) {
//                 var $col = $(col),
//                     text = $col.text();

//                 return text.replace(/"/g, '""'); // escape double quotes

//             }).get().join(tmpColDelim);

//         }).get().join(tmpRowDelim)
//             .split(tmpRowDelim).join(rowDelim)
//             .split(tmpColDelim).join(colDelim) + '"';

//             // Deliberate 'false', see comment below
//     if (false && window.navigator.msSaveBlob) {

//                     var blob = new Blob([decodeURIComponent(csv)], {
//               type: 'text/csv;charset=utf8'
//         });

//         // Crashes in IE 10, IE 11 and Microsoft Edge
//         // See MS Edge Issue #10396033: https://goo.gl/AEiSjJ
//         // Hence, the deliberate 'false'
//         // This is here just for completeness
//         // Remove the 'false' at your own risk
//         window.navigator.msSaveBlob(blob, filename);

//     } else if (window.Blob && window.URL) {
//                     // HTML5 Blob
//         var blob = new Blob([csv], { type: 'text/csv;charset=utf8' });
//         var csvUrl = URL.createObjectURL(blob);

//         $(this)
//                 .attr({
//                     'download': filename,
//                     'href': csvUrl
//                 });
//             } else {
//         // Data URI
//         var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

//                     $(this)
//             .attr({
//                   'download': filename,
//                 'href': csvData,
//                 'target': '_blank'
//                 });
//     }
// }

exportTableToCSV = function ($table, filename) {
    var $headers = $table.find('tr:has(th)')
        ,$rows = $table.find('tr:has(td)')

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        ,tmpColDelim = String.fromCharCode(11) // vertical tab character
        ,tmpRowDelim = String.fromCharCode(0) // null character

        // actual delimiter characters for CSV format
        ,colDelim = '","'
        ,rowDelim = '"\r\n"';

        // Grab text from table into CSV formatted string
        var csv = '"';
        csv += formatRows($headers.map(grabRow));
        // csv += rowDelim;//空白行
        csv += formatRows($rows.map(grabRow)) + '"';

        // Data URI
        // var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
        var csvData = 'data:application/csv;charset=big5,' + EncodedUTF8ToBig5(encodeURIComponent(csv));

    // For IE (tested 10+)
    if (window.navigator.msSaveOrOpenBlob) {
        // var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
        //     type: "text/csv;charset=utf-8;"
        // });
        var blob = new Blob([EncodedUTF8ToBig5(encodeURI(csv))], { // mic: 不確定是對的
            type: "text/csv;charset=big5;"
        });
        navigator.msSaveBlob(blob, filename);
    } else {
        $(this)
            .attr({
                'download': filename
                ,'href': csvData
                //,'target' : '_blank' //if you want it to open in a new window
        });
    }

    //------------------------------------------------------------
    // Helper Functions
    //------------------------------------------------------------
    // Format the output so it has the appropriate delimiters
    function formatRows(rows) {
        return rows.get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim);
    }
    // Grab and format a row from the table
    function grabRow(i,row) {

        var $row = $(row);
        //for some reason $cols = $row.find('td') || $row.find('th') won't work...
        var $cols = $row.find('td');
        if(!$cols.length) $cols = $row.find('th');

        return $cols.map(grabCol)
                    .get().join(tmpColDelim);
    }
    // Grab and format a column from the table
    function grabCol(j,col) {
        var $col = $(col),
            $text = $col.text();

        return $text.replace('"', '""'); // escape double quotes

    }
}
