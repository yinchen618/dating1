Router.configure({
  // we use the  appBody template to define the layout for the entire app
  layoutTemplate: 'mainpage',
  // layoutTemplate: 'appBody',

  // the appNotFound template is used for unknown routes and missing lists
  notFoundTemplate: 'appNotFound',

  // show the appLoading template whilst the subscriptions below load their data
  loadingTemplate: 'appLoading'//,
});


dataReadyHold = null;

if (Meteor.isClient) {
  // Keep showing the launch screen on mobile devices until we have loaded
  // the app's data
  dataReadyHold = LaunchScreen.hold();

  // Show the loading screen on desktop
  // Router.onBeforeAction('loading', {except: ['join', 'signin']});
  // Router.onBeforeAction('dataNotFound', {except: ['join', 'signin']});
}

Router.map(function() {

  this.route('forgotpassword', {
    layoutTemplate: 'mainpage2',
    path: '/forgot-password',
    template: 'ForgotPassword',
  });
  this.route('resetpassword', {
    layoutTemplate: 'mainpage2',
    path: '/#/reset-password/:token',
    template: 'ResetPassword',
  });

  this.route('home', {
    path: '/',
    template: 'home',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Calendar"),
        Meteor.subscribe("Homepics"),
      ];
    }
    // action: function() {
      // Router.go('listsShow', Lists.findOne());

      // console.log("SEMICOLON.documentOnReady.init");
      // SEMICOLON.documentOnLoad.init();
      // SEMICOLON.documentOnReady.init();
      // $window.on('resize', SEMICOLON.documentOnResize.init);

    // }
  });
 /* this.route('home', {
    template: 'home',
    waitOn: function(){
      return [
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }

  });*/
  this.route('login', {
    path: '/login',
    template: 'login',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
      ];
    },
    /*action: function() {
      // Router.go('listsShow', Lists.findOne());
      console.log("logout router");
      Meteor.logout();
      Router.go('/');
    }*/
  });
  this.route('logout', {
    path: '/logout',
    template: 'logout',
    /*action: function() {
      // Router.go('listsShow', Lists.findOne());
      console.log("logout router");
      Meteor.logout();
      Router.go('/');
    }*/
  });
  this.route('interestcondition', {
    path: '/interestcondition',
    template: 'interestcondition',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('fin_arch', {
    path: '/fin_arch/:f1_id',
    template: 'fin_arch',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Booking"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        // Meteor.subscribe("Agents")
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('fin_salary', {
    path: '/fin_salary/:f1_id',
    template: 'fin_salary',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Salary", this.params.f1_id),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct1"),
        // Meteor.subscribe("Oproduct2"),
        // Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        // Meteor.subscribe("Agents"),
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('fin_commission', {
    path: '/fin_commission/:f1_id',
    template: 'fin_commission',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Commission", this.params.f1_id),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('fin_sales', {
    path: '/fin_sales',
    template: 'fin_sales',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("userList"),
        Meteor.subscribe("Fin_sales_year"),
        Meteor.subscribe("Businessgroup"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct3"),
        // // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        // Meteor.subscribe("userList"),
        // Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('fin_chartist', {
    path: '/fin_chartist',
    template: 'fin_chartist',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Businessgroup"),
      ];
    }
  });
  this.route('employeelist', {
    path: '/employeelist',
    template: 'employeelist',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        // Meteor.subscribe("Agents"),
        Meteor.subscribe("Businessgroup"),
      ];
    }
  });
  this.route('followservice', {
    path: '/followservice',
    template: 'followservice',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Nopdfform")
      ];
    }
  });
  this.route('financiallist', {
    path: '/financiallist',
    template: 'financiallist',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Businessgroup"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Accountyear"),
      ];
    }
  });
  this.route('authority', {
    path: '/authority',
    template: 'authority',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        // Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('follownewcase', {
    path: '/follownewcase',
    template: 'follownewcase',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Workdays")
      ];
    }
  });
  this.route('voyasetting', {
    path: '/voyasetting',
    template: 'voyasetting',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('prulifesetting', {
    path: '/prulifesetting',
    template: 'prulifesetting',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('colonialsetting', {
    path: '/colonialsetting',
    template: 'colonialsetting',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('applyservice', {
    path: '/applyservice',
    template: 'applyservice',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Nopdfform")
      ];
    }
  });
  this.route('payreminder', {
    path: '/payreminder',
    template: 'payreminder',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('birthreminder', {
    path: '/birthreminder',
    template: 'birthreminder',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('realestatecount', {
    path: '/realestatecount',
    template: 'realestatecount',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('assetsallocation', {
    path: '/assetsallocation',
    template: 'assetsallocation',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('fundtransfer', {
    path: '/fundtransfer',
    template: 'fundtransfer',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('renewalreminder', {
    path: '/renewalreminder',
    template: 'renewalreminder',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('reportcenter', {
    path: '/reportcenter',
    template: 'reportcenter',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("Reportcenter1"),
        Meteor.subscribe("Reportcenter2"),
      ];
    }
  });
  this.route('formcenter', {
    path: '/formcenter',
    template: 'formcenter',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("Formcenter1"),
        Meteor.subscribe("Formcenter2"),
      ];
    }
  });
  this.route('productinfo', {
    path: '/productinfo',
    template: 'productinfo',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('productcompare', {
    path: '/productcompare',
    template: 'productcompare',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('selemail', {
    path: '/selemail',
    template: 'selemail',
    layoutTemplate: 'empty',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('financial', {
    path: '/financial/:f1_id/:year/:month',
    template: 'financial',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        Meteor.subscribe("Country"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Account1', this.params.f1_id),
        Meteor.subscribe('Account2', this.params.f1_id),
        Meteor.subscribe('Account3', this.params.f1_id),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc', this.params.f1_id),
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('bankacc', {
    path: '/bankacc/:f1_id',
    template: 'bankacc',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Agents"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc'),
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('account', {
    path: '/account/:f1_id',
    template: 'account',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Agents"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Account1'),
        Meteor.subscribe('Account2'),
        Meteor.subscribe('Account3'),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc'),
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('accountyear', {
    path: '/accountyear/:f1_id',
    template: 'accountyear',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Accountyear"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc'),
      ];
    },
    data: function() { return Businessgroup.findOne({_id: this.params.f1_id}); }
  });
  this.route('businessgroup', {
    path: '/businessgroup',
    template: 'businessgroup',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Accountyear"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc'),
      ];
    }
  });
  this.route('provider', {
    path: '/provider',
    template: 'provider',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc'),
      ];
    }
  });
  this.route('announce', {
    path: '/announce',
    template: 'announce',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        // Meteor.subscribe("Product1"),
        // Meteor.subscribe("Product2"),
        // Meteor.subscribe("Product3"),
        // Meteor.subscribe("Product4"),
        // Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe('Account'),
        Meteor.subscribe('Bookingmonth'),
        Meteor.subscribe('Bankacc'),
      ];
    }
  });
  this.route('hrform_management', {
    template: 'hrform_management',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
      ];
    }
  })
  this.route('blank_form', {
    template: 'blank_form',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
      ];
    }
  })
  this.route('hrform', {
    template: 'hrform',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
      ];
    }
  })
  this.route('employee', {
    path: '/employee/:user_id',
    template: 'employee',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Salary"),
        Meteor.subscribe("Businessgroup"),
        Meteor.subscribe("Commission"),
      ];
    },
    data: function() {
      return Meteor.users.findOne({_id: this.params.user_id});
    }

  });

  this.route('hrlist', {
    path: '/hrlist',
    template: 'hrlist',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Businessgroup"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
        Meteor.subscribe("Accountyear"),
      ];
    }
  });
  this.route('dayoff', {
    path: '/dayoff/:f1_id',
    template: 'dayoff',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("Emailauto"),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Dayoff_set"),
        Meteor.subscribe("Dayoff"),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Accountyear", this.params.f1_id),
      ];
    },
    data: function() {
      // return Meteor.users.findOne({_id: this.params.user_id});
      return Businessgroup.findOne({_id: this.params.f1_id});
    }
  });

  this.route('salary_structure', {
    path: '/salary_structure/:f1_id',
    template: 'salary_structure',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("Salary", this.params.f1_id),
      ];
    },
    data: function() {
      // return Meteor.users.findOne({_id: this.params.user_id});
      return Businessgroup.findOne({_id: this.params.f1_id});
    }
  });

  this.route('dayoff_set', {
    path: '/dayoff_set/:f1_id',
    template: 'dayoff_set',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Dayoff"),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
        Meteor.subscribe("Accountyear", this.params.f1_id),
      ];
    },
    data: function() {
      // return Meteor.users.findOne({_id: this.params.user_id});
      return Businessgroup.findOne({_id: this.params.f1_id});
    }
  });

  this.route('dayoff_management', {
    path: '/dayoff_management/:f1_id',
    template: 'dayoff_management',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        Meteor.subscribe("userList"),
        Meteor.subscribe("Accountyear", this.params.f1_id),
        Meteor.subscribe("BusinessgroupOne", this.params.f1_id),
      ];
    },
    data: function() {
      // return Meteor.users.findOne({_id: this.params.user_id});
      return Businessgroup.findOne({_id: this.params.f1_id});
    }
  });

  this.route('user', {
    template: 'user',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }

  });
  this.route('clientslist', {
    template: 'clientslist',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('clientslist2', {
    template: 'clientslist2',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('client', {
    path: '/client/:client_id',
    template: 'client',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Client", this.params.client_id),
        Meteor.subscribe("ClientPortfolio", this.params.client_id),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    },
    data: function() { return Clients.findOne({_id: this.params.client_id}); }
  });
/*  this.route('catagory', {
  });*/
  this.route('product', {
    template: 'product',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('homepics', {
    template: 'homepics',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('oproduct', {
    template: 'oproduct',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('oproduct1', {
    template: 'oproduct1',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }
  });
  this.route('ptemplate', {
    path: '/ptemplate/:template_id',
    template: 'ptemplate',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        // Meteor.subscribe("ClientPortfolio", this.params.client_id),
        // Meteor.subscribe("Portfolio", this.params.portfolio_id),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    },
    data: function() {
      // console.log(this.params.portfolio_id);
      // return Portfolios.findOne({_id: this.params.portfolio_id});
      // return Portfolios.findOne({_id: this.params.portfolio_id});
      // return {
      //   _id: "123"
      // };
    }
  });
  this.route('portfolio', {
    path: '/portfolio/:portfolio_id',
    template: 'portfolio',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        // Meteor.subscribe("ClientPortfolio", this.params.client_id),
        Meteor.subscribe("Portfolio", this.params.portfolio_id),
        Meteor.subscribe("Interestcondition"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    },
    data: function() {
      // console.log(this.params.portfolio_id);
      // return Portfolios.findOne({_id: this.params.portfolio_id});
      return Portfolios.findOne({_id: this.params.portfolio_id});
      // return {
      //   _id: "123"
      // };
    }
  });
  this.route('product4', {
    path: '/product4/:product4_id',
    template: 'product4',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct4One", this.params.product4_id),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Oproduct4Menu"),
        Meteor.subscribe("Oproduct4Photos", this.params.product4_id),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    },
    data: function() {
      return Oproduct4.findOne({_id: this.params.product4_id});
    }
  });
  this.route('product3', {
    path: '/product3/:product3_id',
    template: 'product3',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    },
    // data: function(){}
  });
  this.route('afterservice', {
    template: 'afterservice',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents"),
      ];
    }

  });
  this.route('email', {
    template: 'email',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        Meteor.subscribe("MailTitle"),
        Meteor.subscribe("MailContext"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
  this.route('emailauto', {
    template: 'emailauto',
    layoutTemplate: 'mainpage2',
    waitOn: function(){
      return [
        // Meteor.subscribe("MailTitle"),
        // Meteor.subscribe("MailContext"),
        Meteor.subscribe("Emailauto"),
        Meteor.subscribe("Oproduct1"),
        Meteor.subscribe("Oproduct2"),
        Meteor.subscribe("Oproduct3"),
        // Meteor.subscribe("Oproduct4"),
        Meteor.subscribe("Product1"),
        Meteor.subscribe("Product2"),
        Meteor.subscribe("Product3"),
        Meteor.subscribe("Product4"),
        Meteor.subscribe("Provider"),
        Meteor.subscribe("Agents")
      ];
    }
  });
});
