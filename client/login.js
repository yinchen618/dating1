Template.register.events({
    "submit form": function(event, template) {
        event.preventDefault();
        // console.log("register template");

        // 1. Collect the username and password from the form
        var username = template.find('#rusername').value,
            password = template.find('#rpassword').value,
            email = template.find('#remail').value,
            password2 = template.find('#rpassword2').value,
            chtname = template.find('#chtname').value,
            Phone = template.find('#Phone').value;


        if(password != password2){
            var message = "錯誤: 兩欄密碼不相符";
            $('#rform-messages').html(message);
            return;
        }
        else if(!chtname){
            var message = "錯誤: 請輸入中文姓名";
            $('#rform-messages').html(message);
            return;
        }

        Accounts.createUser({
            username: username,
            password: password,
            email: email,
            profile: {name: chtname, phone: Phone}
        }, function(error) {
            if (error) {
                // console.log(err);
                var message = "錯誤: <strong>" + error.reason + "</strong>";
                $('#rform-messages').html(message);
            } else {
                // console.log('success!');
                Router.go('/');
            }
        });
    }
});
Template.login.helpers({
    /*test: function(){
        // console.log(this);
        return "";
    }*/
});
Template.login.events({
    'submit #login-form': function(event, template) {
        // console.log("form#login-form");
        event.preventDefault();

        // 1. Collect the username and password from the form
        var username = template.find('#lusername').value,
            password = template.find('#lpassword').value;

        Meteor.call("getEmailByWorknumber", username, function(error, result){
            if(error){
                console.log("error from getEmailByWorknumber: ", error);
            }
            else {
                console.log(result);
                // if(result != 0 || result != "" ){
                if(!!result){
                    username = result;
                }

                // 2. Attempt to login.
                Meteor.loginWithPassword(username, password, function(error) {
                    // 3. Handle the response
                    if (Meteor.user()) {
                        // Redirect the user to where they're loggin into. Here, Router.go uses
                        // the iron:router package.

                        Meteor.call("checkIsLoginable", Meteor.userId(), function(error, result){
                            if(error){
                                console.log("error from checkIsLoginable: ", error);
                            }
                            else {
                                if (result == 0) {
                                    alert("本帳號已被停用");
                                    Meteor.logout();
                                    Router.go('login');
                                }
                                else{
                                    Router.go('/');
                                }
                            }
                        });
                    } else {
                        // If no user resulted from the attempt, an error variable will be available
                        // in this callback. We can output the error to the user here.
                        var message = "錯誤: <strong>" + error.reason + "</strong>";
                        $('#lform-messages').html(message);
                    }
                    return;
                });
            }
        });

        return false;
    },
    "submit #register-form": function(event, template) {
        event.preventDefault();
        // console.log("register template");

        // 1. Collect the username and password from the form
        /*var username = template.find('#rusername').value,
            password = template.find('#rpassword').value,
            email = template.find('#remail').value,
            password2 = template.find('#rpassword2').value,
            chtname = template.find('#chtname').value,
            Phone = template.find('#Phone').value;
       */
        var username = $('#rusername').val() || "",
            password = $('#rpassword').val() || "",
            email = $('#remail').val() || "",
            password2 = $('#rpassword2').val() || "",
            // chtname = $('#chtname').val() || "",
            firstname = $('#firstname').val() || "",
            lastname = $('#lastname').val() || "",
            engname = firstname +", "+ lastname,
            chtname = lastname + firstname; //,
            // Phone = $('#Phone').val() || "";

        if(password != password2){
            var message = "錯誤: 兩欄密碼不相符";
            $('#rform-messages').html(message);
            return;
        }
        else if(!chtname){
            var message = "錯誤: 請輸入中文姓名";
            $('#rform-messages').html(message);
            return;
        }
        if(!username){
            username = email;
        }

        Accounts.createUser({
            username: username,
            password: password,
            email: email,
            profile: {
                engname: engname,
                chtname: chtname,
                firstname: firstname,
                lastname: lastname,
                // phone: Phone
            }
        }, function(error) {
            if (error) {
                // console.log(err);
                var message = "錯誤: <strong>" + error.reason + "</strong>";
                $('#rform-messages').html(message);
            } else {
                // console.log('success!');
                Router.go('/');
            }
        });
    }
});

Template.login.rendered = function() {
    Template.semicolon.loadjs();
};
Template.logout.rendered = function() {
    // console.log("logout");

    Meteor.logout();
    // Router.go('/');
    Router.go('login');
};

Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
});
