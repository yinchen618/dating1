// Do not forget to add the email package: $ meteor add email
// and to configure the SMTP: https://gist.github.com/LeCoupa/9879221

Template.ForgotPassword.events({
    'submit #forgotPasswordForm': function(e, t) {
        e.preventDefault();

        if(!confirm("確定要重設密碼?")){
        	return;
        }
        $("#result").text('處理中…');


        var forgotPasswordForm = $(e.currentTarget),
            email = forgotPasswordForm.find('#forgotPasswordEmail').val().toLowerCase().trim();

        // if (isNotEmpty(email) && isEmail(email)) {

        Accounts.forgotPassword({ email: email }, function(err) {
            if (err) {
                if (err.message === 'User not found [403]') {
                    // console.log('This email does not exist.');
                    $("#result").text('無此 Email');
                }
                else {
                    // console.log('We are sorry but something went wrong.');
                    $("#result").text('We are sorry but something went wrong.');
                    console.log(err);
                }
            } 
            else {
                // console.log('Email Sent. Check your mailbox.');
                $("#result").text('Email 已送出。請檢查您的電子信箱。');
            }
        });

        // }
        return false;
    },
});

if (Accounts._resetPasswordToken) {
    Session.set('resetPassword', Accounts._resetPasswordToken);
}

Template.ResetPassword.helpers({
    resetPassword: function() {
        return Session.get('resetPassword');
    }
});

Template.ResetPassword.events({
    'submit #resetPasswordForm': function(e, t) {
        e.preventDefault();

        var resetPasswordForm = $(e.currentTarget),
            password = resetPasswordForm.find('#resetPasswordPassword').val(),
            passwordConfirm = resetPasswordForm.find('#resetPasswordPasswordConfirm').val();

        if (isNotEmpty(password) && areValidPasswords(password, passwordConfirm)) {
            Accounts.resetPassword(Session.get('resetPassword'), password, function(err) {
                if (err) {
                    console.log('We are sorry but something went wrong.');
                } else {
                    console.log('Your password has been changed. Welcome back!');
                    Session.set('resetPassword', null);
                }
            });
        }
        return false;
    }
});
