Template.salary_excel.rendered = function(){
    Template.semicolon.loadjs();

    $("#e_salary_excel").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        filtering: true,
        editing: false,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "salary_excel", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "salary_excel", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "salary_excel", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "salary_excel", "DELETE");
            },
        },
        fields: [
            { name: "worknumber", title: "工號", type: "text"},
            { name: "", title: "員工姓名"},
            { name: "", title: "職稱"},
            { name: "", title: "就職日期"},
            { name: "", title: "加項<br>本薪"},
            { name: "", title: "加項<br>津貼"},
            { name: "", title: "減項<br>應扣薪資"},
            { name: "", title: "減項<br>健保"},
            { name: "", title: "減項<br>勞保"},
            { name: "", title: "實發金額"},
            { name: "", title: "付款銀行<br>上海商銀"},
            { name: "", title: "付款銀行<br>台北富邦"},
            { name: "", title: "付款銀行<br>香港匯豐"},
            { name: "", title: "備註"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_salary_excel").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_salary_excel").jsGrid("loadData");
        },
    });
};
Template.salary_excel.helpers({
    acc_year: Accountyear.find({}, {sort:{value:1}}),

    get_objDay_off: function() {
        return objDay_off;
    },

    get_bjDay_off_status: function() {
        return objDay_off_status;
    },
});

Template.salary_excel.events({
    'click .btn-new_1': function() {
        var formVar = {};

        $("#e_dayoff").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
            } else {
              }
        });
    },
});
