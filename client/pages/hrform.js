Template.hrform.rendered = function(){
    Template.semicolon.loadjs();
    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: false,
        pageSize: 20,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                filter.display = "2";
                return jsgridAjax(filter, "hrform_management", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "hrform_management", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "hrform_management", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "hrform_management", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:50, },
            { name: "name", title: "檔案名稱", type: "text", align: "center"},
            { name: "preview", width:50, align:"center", title: "預覽",
                itemTemplate: function(value, item) {
                    var str = "";
                    if(!!item.url){
                        str = str + '<a href="#" data-num="1" data-toggle="modal" data-target="#myModalFile" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+"[預覽]"+'</a>';
                        str = str + " ";
                    }
                    return str;
                }
            },
            // { name: "display", title: "是否顯示於人事規章", width: 60, type: "select", items: [{ id: "2", value: "顯示" },{ id: "3", value: "不顯示" }], valueField: "id", textField: "value"  },
            { name: "remark", title: "備註", width:60, type:"text", align: "center"},
        ],
    });
    $('#myModalFile').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var index = button.data("index");
        var data = $("#cFiles").data("JSGrid").data[index];
        var url = button.data("url");
        $("#file_iframe").attr("src", url);
    });
    //預覽文件,關閉modal
    $('#myModalFile').on('hide.bs.modal', function (event) {
    $("#file_iframe").attr("src", "");
    });
};
Template.hrform.helpers({
});

Template.hrform.events({
});