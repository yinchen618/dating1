Template.formcenter.rendered = function(){
    Template.semicolon.loadjs();
    //文件類別表單
    $("#e_formcenter1").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "formcenter1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "formcenter1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "formcenter1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "formcenter1", "DELETE");
            },
        },
        fields: [
            { type: "control", title: "刪除", width:40, },
            { name: "catalog", title: "文件類別", type: "text", width:100,},
            // { name: "blank_doc", title: "空白檔案", type: "text", width:80},
            // { name: "remark", title: "備註", type: "text", width:60 },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_formcenter1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_formcenter1").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#e_formcenter1 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortdFormcenter1", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_formcenter1").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    //下方顯示表單
    $("#e_formcenter2").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
        		if(!!Session.get('formcenter1')){
	        		var f1 = Session.get('formcenter1');
	        		filter.f1 = f1;
        		}
                return jsgridAjax(filter, "formcenter2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "formcenter2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "formcenter2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "formcenter2", "DELETE");
            },
        },
        fields: [
            { type: "control", title: "刪除", width:30},
            { name: "f1", title: "文件類別", type: "select", items: Formcenter1.find({},{sort:{order_id: 1}}).fetch(), valueField: "_id", textField: "catalog" },
            { name: "apply_doc", title: "申請文件", width:80, type:"text" },
            { name: "blank_doc", title: "空白檔案", width:80, editing:false,
        		itemTemplate: function(value, item) {
                    var str = "";
                    if(!!item.file1_url){
						str = str + '<a href="#" data-num="1" data-toggle="modal" data-target="#myModalFile" data-url="'+item.file1_url+'" data-index="'+item.myItemIndex+'">'+"[預覽]"+'</a>';
	                    str = str + " ";
						// str = str + '<a href="'+item.file1_url+'" target="_blank">'+"[下載]"+'</a>';
                    }
                    return str;
                }
            },
            { name: "example_doc", title: "範例檔案", editing:false, width:80,
				itemTemplate: function(value, item) {
                    var str = "";

                    if(!!item.file2_url){
						str = str + '<a href="#" data-num="2" data-toggle="modal" data-target="#myModalFile" data-url="'+item.file2_url+'" data-index="'+item.myItemIndex+'">'+"[預覽]"+'</a>';
                    	str = str + " ";
						// str = str + '<a href="'+item.file2_url+'" target="_blank">'+"[下載]"+'</a>';
                    }
                    return str;
                }
        	},
            { name: "remind_doc", title: "附加文件提醒", width:60, type:"text"},
            { name: "workingdays", title: "工作天數", width:40, type:"text", align: ""},
            { name: "remark", title: "備註", width:60, type:"text"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_formcenter2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_formcenter2").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#e_formcenter2 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortdFormcenter2", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_formcenter2").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    //文件類別,切換顯示下方表單
    var val = $("#sel-catalog").find("option:selected").val();
    Session.set('formcenter1', val);
    $("#e_formcenter2").jsGrid("loadData");
    //文件類別管理,打開時顯示表單
    $('#myModal1').on('shown.bs.modal', function (event) {
        $("#e_formcenter1").jsGrid("loadData");
    });
    //文件類別管理,關閉時清空input
    $('#myModal1').on('hide.bs.modal', function (event) {
        $("#catalog").val("");
    });
    //新增文件,打開時選好文件類別
    $('#myModal2').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $("#m2-f1").val( $("#sel-catalog option:selected").val() );
        $("#m2-f1_text").val( $("#sel-catalog option:selected").text() );
        $("#m2-f1_showtext").text( $("#sel-catalog option:selected").text() );
    });
    //新增文件,關閉時清空input
    $('#myModal2').on('show.bs.modal', function (event) {
        $("#apply_doc").val("");
        $("#blank_doc").val("");
        $("#example_doc").val("");
        $("#remind_doc").val("");
        $("#workingdays").val("");
        $("#remark").val("");
        $("#m2-file1_url").val("");
        $("#m2-file2_url").val("");
    });
    //新增文件,瀏覽資料按鈕
    $("#input-3").fileinput({
        'language': "zh-TW",
    });
    $("#input-4").fileinput({
        'language': "zh-TW",
    });
    $("#input-5").fileinput({
        'language': "zh-TW",
    });
    $("#input-6").fileinput({
        'language': "zh-TW",
    });
    //預覽文件,打開modal
    $('#myModalFile').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var index = button.data("index");
        var data = $("#e_formcenter2").data("JSGrid").data[index];
        var url = button.data("url");
        /*if(url.isArray){
            url = url[0];
        }
        url = url.replace(/^"(.*)"$/, '$1');
        if(url.slice(-1) == ","){
            url = url.slice(0,-1);
        }*/
        $("#file_iframe").attr("src", url);
    });
    //預覽文件,關閉modal
    $('#myModalFile').on('hide.bs.modal', function (event) {
    $("#file_iframe").attr("src", "");
    });
};
Template.formcenter.helpers({
    get_catalog: Formcenter1.find({}, {sort:{order_id: 1}}),
    get_catalog_count: function(){ return Formcenter1.find({}, {sort:{order_id: 1}}).count(); },
    "files1": function(){
        return S3.collection.find({uploader: "1"});
    },
    "files2": function(){
        return S3.collection.find({uploader: "2"});
    }
});
Template.formcenter.events({
    //切換類別,下方表單切換
	'change #sel-catalog': function(event) {
		// console.log(event);
		var val = $(event.currentTarget).find("option:selected").val();
        Session.set('formcenter1', val);
        $("#e_formcenter2").jsGrid("loadData");
    },
    //文件類別管理
    'submit .form-modal1': function(event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal1").serializeObject();
        $("#e_formcenter1").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                // $('#myModal1').modal('hide');
                $(".form-modal1").trigger('reset');
                $("#e_formcenter1").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    //新增文件
    'submit .form-modal2': function(event) {
        $(".progress").hide();
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal2").serializeObject();
        $("#e_formcenter2").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#myModal2').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#e_formcenter2").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
        // location.reload();
    },
    //上傳空白檔案
    "click a.upload1": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                uploader: "1",
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {
                formcenter: "1",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#m2-file1_url").val(r.secure_url);
        });
    },
    //上傳範例檔案
    "click a.upload2": function(){
        var files = $("input.file_bag2")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                uploader: "2",
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-4').fileinput('clear');

            var formVar = {
                formcenter: "2",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#m2-file2_url").val(r.secure_url);
        });
    },
});
