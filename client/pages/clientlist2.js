Template.clientslist2.rendered = function() {
  Template.semicolon.loadjs();

  $("#clients").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    noDataContent: "尚無資料",
    sorting: true,
    paging: true,
    // filtering: true,
    // editing: true,
    pageSize: 20,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        filter.findData = $("input#search_text").val() || "";
        return jsgridAjax(filter, "clients", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "clients", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "clients", "PUT");

      },
      deleteItem: function(item) {
        return jsgridAjax(item, "clients", "DELETE");
      },
    },
    fields: [{
        name: "uid",
        title: "客戶編號",
        type: "text",
        width: 60,
        align: "center",
        itemTemplate: function(value, item) {
          return '<a target="_blank" href="/client/' + item._id + '">' + item.uid + '</a>';
        }
      },
      {
        name: "name_cht",
        title: "中文名",
        type: "text",
        width: 70,
        align: "center"
      },
      {
        name: "name_eng",
        title: "英文名",
        type: "text",
        width: 80
      },
      {
        name: "sexual_text",
        title: "性別",
        type: "text",
        width: 40,
        align: "center"
      },
      {
        name: "birthday",
        title: "生日",
        type: "text",
        width: 80,
        align: "center"
      },
      {
        name: "cellnum",
        title: "行動電話",
        type: "text",
        width: 80
      },
      {
        name: "email",
        title: "電子信箱",
        type: "text"
      },
    ],
  });
};

Template.clientslist2.helpers({});

Template.clientslist2.loadPortfolio = function() {
  $("#clients").jsGrid("loadData");
  $("#clients").jsGrid("openPage", 1);
}
Template.clientslist2.events({
  'change #search_text, keypress #search_text, click #search_text': function(event) { //#
    Template.clientslist2.loadPortfolio();
  },
});
