Template.provider.rendered = function(){
    Template.semicolon.loadjs();
    $("#e_provider").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "provider", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "provider", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "provider", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "provider", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, editButton: false },
            // { name: "uid", title: "#", width: 60},
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
            { name: "name_cht", title: "中文名稱", type: "text"},
            { name: "name_eng", title: "英文名稱", type: "text"},
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_provider").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_provider").jsGrid("loadData");
        },
    });
};
Template.provider.helpers({
    get_countries: function() {
        return objCountries;
    },
});

Template.provider.events({
    'click .btn-new': function() {
        var formVar = {};
        $("#e_provider").jsGrid("insertItem", formVar).done(function(ret) {

            if (ret.insert == "success") {
            } else {
              }
        });
    },
});
