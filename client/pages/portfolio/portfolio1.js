Template.portfolio1.rendered = function(){
    // Template.semicolon.loadjs();

    var data = Blaze.getData();

    $("#account_num").text(data.account_num);
    $("#nowstatus").text(funcObjFind2(objNowStatus, data.nowstatus));
    $("#assume_insure_price").text(data.assume_insure_price);
    $("#effective_date").text(data.effective_date);
    $("#confirmed_insure_price").text(data.confirmed_insure_price);
    $("#backdate").text(data.backdate);
    $("#temp_applybook_date").text(data.temp_applybook_date);
    // $("#pay_year_num").text(data.pay_year_num);
    $("#pay_year_num").text(funcObjFind2(objPayYearNum, data.pay_year_num));
    $("#telephone_credit").text(data.telephone_credit);
    $("#bodycheck_date").text(data.bodycheck_date);
    $("#cpaletter_date").text(data.cpaletter_date);
    $("#bodycheck_place").text(data.bodycheck_place);
    $("#tp").text(data.tp);
    $("#sign_date").text(data.sign_date);
    $("#bodychecktype3").text(funcObjFind2(objBodyCheckType3, data.bodychecktype1));
    $("#insurance_receive_date").text(data.insurance_receive_date);
    $("#is_trust").text(funcObjFind2(objYN, data.is_trust));
    $("#insurance_give_date").text(data.insurance_give_date);
    $("#trust_isrevoke").text(funcObjFind2(objIsRevoke, data.trust_isrevoke));
    $("#onlineaccess_account").text(data.onlineaccess_account);
    $("#onlineaccess_password").text(data.onlineaccess_password);
    $("#trust_name").text(data.trust_name);
    $("#trust_trustee").text(data.trust_trustee);
    $("#trust_grantor").text(data.trust_grantor);
    $("#trust_ssnitin").text(data.trust_ssnitin);
    $("#provider_id").text(Provider.findOne({_id:data.provider_id}).name_cht || "");
    $("#trust_beneficial1_name")        .text(data.trust_beneficial1_name);
    $("#trust_beneficial1_percent")     .text(data.trust_beneficial1_percent);
    $("#trust_beneficial1_relationship").text(data.trust_beneficial1_relationship);
    $("#trust_beneficial2_name")        .text(data.trust_beneficial2_name);
    $("#trust_beneficial2_percent")     .text(data.trust_beneficial2_percent);
    $("#trust_beneficial2_relationship").text(data.trust_beneficial2_relationship);
    $("#trust_beneficial3_name")        .text(data.trust_beneficial3_name);
    $("#trust_beneficial3_percent")     .text(data.trust_beneficial3_percent);
    $("#trust_beneficial3_relationship").text(data.trust_beneficial3_relationship);
    $("#trust_beneficial4_name")        .text(data.trust_beneficial4_name);
    $("#trust_beneficial4_percent")     .text(data.trust_beneficial4_percent);
    $("#trust_beneficial4_relationship").text(data.trust_beneficial4_relationship);
    $("#nowphase").text(funcObjFind2(objNowPhase, data.nowphase));
    $("#lockmonth").text(funcObjFind2(objLockMonth, data.lockmonth));

    $("#trust_bankaccount").text(data.trust_bankaccount || "Law Offices of Pyng Soon, Inc. IOLTA");
    $("#trust_account").text(data.trust_account || "63878607");
    $("#trust_routenum").text(data.trust_routenum || "322070381");
    $("#e_followservice").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                filter.portfolio_id = data._id;
                filter.pf6_status = "1"
                return jsgridAjax(filter, "afterservice", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "afterservice", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "afterservice", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "afterservice", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:60, editButton: false },
            { name: "apply_btn", title: "#", type: "text", width:60, align: "center",
                itemTemplate: function(value, item) {
                    // console.log(value);
                    // console.log(item);
                    return '<a href="#" data-toggle="modal" data-target="#myModal" data-index="'+item.myItemIndex+'">'+item.uid+'</a>';
                }
            },
            { name: "p_uid", title: "Portfolio", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.portfolio && !!item.portfolio.uid)
                        return '<a href="/portfolio/'+item.portfolio._id+'" target="_blank">'+item.portfolio.uid+'</a>';
                    else
                        return "";
                },
            },
            // { name: "provider_text", title: "Provider", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.provider_engtext)
            //             return item.portfolio.provider_engtext;
            //         else
            //             return "";
            //     },
            // },
            { name: "insertedAt", title: "Date", width:100,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
            // { name: "agent_text", title: "Agent Name", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.agent_text)
            //             return item.portfolio.agent_text;
            //         else
            //             return "";
            //     },
            // },
            // { name: "name_eng", title: "Client Name", type: "text",
            //     // itemTemplate: function(value, item) {
            //     //     if(!!item.insertedAt)
            //     //         return item.provider_engtext;
            //     //     else
            //     //         return "";
            //     // },
            // },
            { name: "policy_num", title: "Service Item", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.product4_engtext)
                        return item.product4_engtext;
                    else
                        return "";
                },
            },
            { name: "status", title: "Status", type: "text",
              itemTemplate: function(value, item) {
                return funcObjFind(objASPhase, value)
              }
            },
            { name: "description", title: "Description", type: "text"},
            { name: "as_template_text", title: "Action", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            { name: "as_owner_text", title: "Action Owner", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            // { name: "contactnum", title: "", type: "text"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_account").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account").jsGrid("loadData");
        },
    });
    Template.portfolio.loadForm();
};

Template.portfolio1.helpers({
  // homepics: objHomePics
    nowUpdatedAt: function(){
        return (this.updatedAt || this.insertedAt).yyyymmddhm();
    },
});

Template.portfolio1.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
});
