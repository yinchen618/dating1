Template.portfolio7.rendered = function(){
    var data = Blaze.getData();

    $("#certificate_name").text(data.certificate_name);
    $("#invest_money").text(data.invest_money);
    $("#certificate_no").text(data.certificate_no);
    $("#payment_date").text(data.payment_date);
    $("#effective_date").text(data.effective_date);
    $("#payment_method").text(data.payment_method);
    $("#receive_money_date").text(data.receive_money_date);
    $("#nowphase").text(funcObjFind2(objNowPhase, data.nowphase));
    $("#stockrightsummit").text(data.stockrightsummit);

    $("#beneficiary_bank_location").text(data.beneficiary_bank_location);
    $("#beneficiary_bank_name").text(data.beneficiary_bank_name);
    $("#beneficiary_bank_address").text(data.beneficiary_bank_address);
    $("#swift_code").text(data.swift_code);
    $("#beneficiary_account_number").text(data.beneficiary_account_number);
    $("#beneficiary_name").text(data.beneficiary_name);
    $("#lockmonth").text(funcObjFind2(objLockMonth, data.lockmonth));

    Template.portfolio.loadForm();
    $("#e_followservice").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                filter.portfolio_id = data._id;
                filter.pf6_status = "1"
                return jsgridAjax(filter, "afterservice", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "afterservice", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "afterservice", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "afterservice", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:60, editButton: false },
            { name: "apply_btn", title: "#", type: "text", width:60, align: "center",
                itemTemplate: function(value, item) {
                    // console.log(value);
                    // console.log(item);
                    return '<a href="#" data-toggle="modal" data-target="#myModal" data-index="'+item.myItemIndex+'">'+item.uid+'</a>';
                }
            },
            { name: "p_uid", title: "Portfolio", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.portfolio && !!item.portfolio.uid)
                        return '<a href="/portfolio/'+item.portfolio._id+'" target="_blank">'+item.portfolio.uid+'</a>';
                    else
                        return "";
                },
            },
            // { name: "provider_text", title: "Provider", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.provider_engtext)
            //             return item.portfolio.provider_engtext;
            //         else
            //             return "";
            //     },
            // },
            { name: "insertedAt", title: "Date", width:100,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
            // { name: "agent_text", title: "Agent Name", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.agent_text)
            //             return item.portfolio.agent_text;
            //         else
            //             return "";
            //     },
            // },
            // { name: "name_eng", title: "Client Name", type: "text",
            //     // itemTemplate: function(value, item) {
            //     //     if(!!item.insertedAt)
            //     //         return item.provider_engtext;
            //     //     else
            //     //         return "";
            //     // },
            // },
            { name: "policy_num", title: "Service Item", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.product4_engtext)
                        return item.product4_engtext;
                    else
                        return "";
                },
            },
            { name: "status", title: "Status", type: "text",
              itemTemplate: function(value, item) {
                return funcObjFind(objASPhase, value)
              }
            },
            { name: "description", title: "Description", type: "text"},
            { name: "as_template_text", title: "Action", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            { name: "as_owner_text", title: "Action Owner", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            // { name: "contactnum", title: "", type: "text"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_account").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account").jsGrid("loadData");
        },
    });
};

Template.portfolio7.helpers({
  // homepics: objHomePics
    nowUpdatedAt: function(){
        return (this.updatedAt || this.insertedAt).yyyymmddhm();
    },
});

Template.portfolio7.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
});
