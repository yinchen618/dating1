Template.portfolio5.rendered = function(){

    var data = Blaze.getData();

    $("#account_num").text(data.account_num);
    $("#prebuybook_date").text(data.prebuybook_date);
    $("#invest_type").text(funcObjFind2(objInvestType, data.invest_type));
    $("#givecontract_date").text(data.givecontract_date);
    $("#company_type").text(funcObjFind2(objCompanyType, data.company_type));
    $("#return_principal_date").text(data.return_principal_date);
    $("#invest_money").text(data.invest_money);
    $("#effective_date").text(data.effective_date);
    $("#payment_date").text(data.payment_date);
    $("#payment_money").text(data.payment_money);
    $("#payment_moneytype").text(funcObjFind2(objSalaryCash, data.payment_moneytype));
    $("#payment_date2").text(data.payment_date2);
    $("#payment_money2").text(data.payment_money2);
    $("#payment_moneytype2").text(funcObjFind2(objSalaryCash, data.payment_moneytype2));
    $("#payment_date3").text(data.payment_date3);
    $("#payment_money3").text(data.payment_money3);
    $("#payment_moneytype3").text(funcObjFind2(objSalaryCash, data.payment_moneytype3));
    $("#payment_method").text(data.payment_method);
    $("#itin_no").text(funcObjFind2(objYN, data.itin_no));
    $("#receive_money_date").text(data.receive_money_date);
    // $("#interest_condition").text(data.interest_condition);
    // console.log(data.interest_condition);
    if(!!data.interest_condition && Interestcondition.find({_id: data.interest_condition}).count() ){
        $("#interest_condition").text(Interestcondition.findOne({_id: data.interest_condition}).interestcondition || "");
    }
    $("#pay_year_num").text(data.pay_year_num);
    // $("#interest_period").text(funcObjFind2(objICPeriod, data.interest_period));
    $("#giveinterest_period").text(funcObjFind2(objPaymentFreqYear, data.giveinterest_period));
    $("#giveinterest_month").text(funcObjFind2(objPayMonthNum, data.giveinterest_month));
    $("#payMonthNum").text(funcObjFind2(objPayMonthNum, data.payMonthNum));
    $("#nowphase").text(funcObjFind2(objNowPhase, data.nowphase));
    $("#provider_id").text(Provider.findOne({_id:data.provider_id}).name_cht || "");

    $("#beneficiary_bank_location").text(data.beneficiary_bank_location);
    $("#beneficiary_bank_name").text(data.beneficiary_bank_name);
    $("#beneficiary_bank_address").text(data.beneficiary_bank_address);
    $("#swift_code").text(data.swift_code);
    $("#beneficiary_account_number").text(data.beneficiary_account_number);
    $("#beneficiary_name").text(data.beneficiary_name);
    $("#lockmonth").text(funcObjFind2(objLockMonth, data.lockmonth));

    $("#is_leave").text(funcObjFind2(objYN, data.is_leave));
    $("#leavetotal_money").text(data.leavetotal_money);

    $("#irs_w7_sign_date").text(data.irs_w7_sign_date);
    $("#irs_w7_status").text(funcObjFind2(objStatusP5, data.irs_w7_status));
    $("#irs_w7_sent_date").text(data.irs_w7_sent_date);
    $("#irs_ps").text(data.irs_ps);

    $("#certpassport_use_date").text(data.certpassport_use_date);
    $("#certpassport_status").text(funcObjFind2(objStatusP5, data.certpassport_status));
    $("#certpassport_sent_date").text(data.certpassport_sent_date);
    $("#certpassport_ps").text(data.certpassport_ps);

    $("#itin_text").text(data.itin_text);
    $("#itin_status").text(funcObjFind2(objStatusP5, data.itin_status));
    $("#itin_date").text(data.itin_date);
    $("#itin_ps").text(data.itin_ps);

    $("#t2848_text").text(data.t2848_text);
    $("#taxret_text").text(data.taxret_text);
    $("#collectdone_date").text(data.collectdone_date);
    $("#assumeinvest_year").text(data.assumeinvest_year);
    $("#assumeleave_year").text(funcObjFind2(objYearNum, data.assumeleave_year));
    $("#assumeleave_season").text(funcObjFind2(objSeason, data.assumeleave_season));
    $("#p5Country").text(funcObjFind2(objP5Country, data.p5Country));
    $("#p5Dollar").text(funcObjFind2(objP5Dollar, data.p5Dollar));

    Template.portfolio.loadForm();
    $("#e_followservice").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller: {
            loadData: function(filter) {
                filter.portfolio_id = data._id;
                filter.pf6_status = "1"
                return jsgridAjax(filter, "afterservice", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "afterservice", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "afterservice", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "afterservice", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:60, editButton: false },
            { name: "apply_btn", title: "#", type: "text", width:60, align: "center",
                itemTemplate: function(value, item) {
                    // console.log(value);
                    // console.log(item);
                    return '<a href="#" data-toggle="modal" data-target="#myModal" data-index="'+item.myItemIndex+'">'+item.uid+'</a>';
                }
            },
            { name: "p_uid", title: "Portfolio", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.portfolio && !!item.portfolio.uid)
                        return '<a href="/portfolio/'+item.portfolio._id+'" target="_blank">'+item.portfolio.uid+'</a>';
                    else
                        return "";
                },
            },
            // { name: "provider_text", title: "Provider", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.provider_engtext)
            //             return item.portfolio.provider_engtext;
            //         else
            //             return "";
            //     },
            // },
            { name: "insertedAt", title: "Date", width:100,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
            // { name: "agent_text", title: "Agent Name", type: "text",
            //     itemTemplate: function(value, item) {
            //         if(!!item.portfolio && !!item.portfolio.agent_text)
            //             return item.portfolio.agent_text;
            //         else
            //             return "";
            //     },
            // },
            // { name: "name_eng", title: "Client Name", type: "text",
            //     // itemTemplate: function(value, item) {
            //     //     if(!!item.insertedAt)
            //     //         return item.provider_engtext;
            //     //     else
            //     //         return "";
            //     // },
            // },
            { name: "policy_num", title: "Service Item", type: "text",
                itemTemplate: function(value, item) {
                    if(!!item.product4_engtext)
                        return item.product4_engtext;
                    else
                        return "";
                },
            },
            { name: "status", title: "Status", type: "text",
              itemTemplate: function(value, item) {
                return funcObjFind(objASPhase, value)
              }
            },
            { name: "description", title: "Description", type: "text"},
            { name: "as_template_text", title: "Action", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            { name: "as_owner_text", title: "Action Owner", type: "text",
                // itemTemplate: function(value, item) {
                //     if(!!item.insertedAt)
                //         return item.provider_engtext;
                //     else
                //         return "";
                // },
            },
            // { name: "contactnum", title: "", type: "text"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_account").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account").jsGrid("loadData");
        },
    });
};

Template.portfolio5.helpers({
  // homepics: objHomePics
    nowUpdatedAt: function(){
        return (this.updatedAt || this.insertedAt).yyyymmddhm();
    },
    isFixedInterest: function(){
        console.log(this);
        var ic = Interestcondition.findOne({_id: this.interest_condition});
        if(!!ic){
            if(Number(ic.interest_period) == 0){
                return 1;
            }
            else
                return 0;
        }
        return 0;
    },
});

Template.portfolio5.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
});
