Template.employeelist.rendered = function(){
    Template.semicolon.loadjs();
    $("#employeelist").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        sorting: true,
        paging: true,
        // filtering: true,
        // editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var sel_auth = $("#sel_filter").find("option:selected").val();
                if (sel_auth == "1" || !sel_auth) {
                    filter.is_auth = "1"
                } else if (sel_auth == "0") {
                    filter.is_auth = "0"
                } else if (sel_auth == "3") {}
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }
                return jsgridAjax(filter, "employeelist", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "employeelist", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "employeelist", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "employeelist", "DELETE");
            },
        },
        fields: [
            { name: "worknumber", title: "工號", type: "text", align: "center",
                // itemTemplate: function(value, item) {
                //     // console.log(item);
                //     // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                //     // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                //     return '<a target="_blank" href="/employee/'+item._id+'">'+item.worknumber+'</a>';
                // },
            },
            { name: "chtname", title: "中文姓名", type: "text", align: "center"},
            { name: "engname", title: "英文姓名", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    // return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                    return '<a target="_blank" href="/employee/'+item._id+'">'+item.engname+'</a>';
                },
            },
            { name: "onbroad_date", title: "就職日期", type: "text", align: "center"},
            { name: "jobtitle", title: "職稱", type: "text", align: "center"},
            // { name: "tw_id", title: "身分證字號", type: "text", align: "center"},
            { name: "birthday_date", title: "生日", type: "text", align: "center"},
            { name: "cellphone", title: "連絡電話", type: "text", align: "center"},
            { name: "email", title: "E-Mail(主)", type: "text", align: "center"},
            // { name: "email2", title: "E-Mail(次要)", type: "text", align: "center"},
            { name: "aan", title: "AAN", type: "text", align: "center"},
            { name: "department_id", title: "隸屬事業群", type: "text", align: "center",
                itemTemplate: function(value, item) {
                    var bg = Businessgroup.findOne({_id:item.department_id});
                    if (!!bg) {
                        return Businessgroup.findOne({_id:item.department_id}).value;
                    } else {
                        return "";
                    }
                }
            },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#employeelist").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#employeelist").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#employeelist .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortEmployee", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#employeelist").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
};
Template.employeelist.helpers({
    get_countries: function() {
        return objCountries;
    },
});
Template.employeelist.events({
    "change #sel_filter": function(event, template) {
        $("#employeelist").jsGrid("loadData");
    },
});