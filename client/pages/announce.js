Template.announce.rendered = function() {
    Template.semicolon.loadjs();
    // var $ = jQuery.noConflict();
    $("#calendar").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
    			if(!filter.sortField){
                    filter.sortField = "publishAt_time";
                    filter.sortOrder = "desc";
                }
                return jsgridAjax(filter, "calendar", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "calendar", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "calendar", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "calendar", "DELETE");
            },
        },
        fields: [
            { type: "control", width: 60, editButton: false },
            // { name: "towhom_text", title: "發佈對象", width: 60 },
            { name: "towhom", title: "發佈對象", width: 60, type: "select", items: [{ id: "2", value: "外部" },{ id: "3", value: "內部" }], valueField: "id", textField: "value"  },
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
            { name: "value", title: "公告內容", type: "text" },
            { name: "publishAt_time", title: "公告時間", type: "text" },
            { name: "insertedAt_time", title: "新增時間", type: "text", editing:false },
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#calendar").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#calendar").jsGrid("loadData");
        },
    });
};

Template.announce.helpers({
    // homepics: objHomePics
});

Template.announce.events({
    'click .btn-new': function(event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal2").serializeObject();

        formVar.isopen = 1;
        formVar.insertedAt_date = new Date().yyyymmdd();
        formVar.insertedAt_time = new Date().yyyymmddhm();
        formVar.publishAt_time = new Date().yyyymmddhm();
        formVar.insertedAt = new Date();
        formVar.towhom_text = $("#towhom option:selected").text();

        $("#calendar").jsGrid("insertItem", formVar).done(function(ret) {

            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#myModal').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#calendar").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
    },
});
