Template.oproduct.loadP4 = function(id) {
    // var id = Session.get("nowrow_oproduct4")._id;
    // console.log("Template.oproduct.loadP4: "+ id);
    $("#cFiles").jsGrid("loadData");

    // Meteor.call("getP4obj", id, function(error, result){
        // var p4 = result; // Session.get("nowrow_oproduct4");
        var p4 = Oproduct4.findOne({_id: id});

        // console.log("p4: ");
        // console.log(p4);

        // $("#pic1").empty();
        // var img = $('<img>').attr({
        //     src: p4.showpic1,
        // });
        // $("#pic1").append(img);

        $("#op4-big-pic").attr("src", p4.showpic1 || "");
        $("#op4-big-pic2").attr("src", p4.showpic2 || "");
        $("#title1").val(p4.title1);
        $("#title2").val(p4.title2);
        $("#youtube_url").val(p4.youtube_url);
        $("#video_url").val(p4.video_url);

        $("#intro_title1").val(p4.intro_title1);
        // $("#intro_content1").val(p4.intro_content1);
        $("#intro_title2").val(p4.intro_title2);
        // $("#intro_content2").val(p4.intro_content2);
        $("#intro_title3").val(p4.intro_title3);
        // $("#intro_content3").val(p4.intro_content3);

        $("#youtube_title").val(p4.youtube_title);
        $("#youtube_content").val(p4.youtube_content);
        $("#video_title").val(p4.video_title);
        $("#video_content").val(p4.video_content);
/*
        $('#intro_content1').summernote({
            lang: 'zh-TW', dialogsInBody: true
        });
        $('#intro_content1').summernote('code', p4.intro_content1 || "");

        $('#intro_content2').summernote({
            lang: 'zh-TW', dialogsInBody: true
        });
        $('#intro_content2').summernote('code', p4.intro_content2 || "");

        $('#intro_content3').summernote({
            lang: 'zh-TW', dialogsInBody: true
        });
        $('#intro_content3').summernote('code', p4.intro_content3 || "");*/

        CKEDITOR.config.removePlugins = 'forms';
        CKEDITOR.config.removeButtons = 'Save,Print,Preview,Find,About,Maximize,ShowBlocks';


        CKEDITOR.replace('intro_content1');
        CKEDITOR.replace('intro_content2');
        CKEDITOR.replace('intro_content3');


        CKEDITOR.instances["intro_content1"].setData(p4.intro_content1);
        CKEDITOR.instances["intro_content2"].setData(p4.intro_content2);
        CKEDITOR.instances["intro_content3"].setData(p4.intro_content3);


        $('.dropdown-toggle').dropdown();
}
Template.oproduct.rendered = function() {
    Session.set("nowrow_oproduct4", "");
    Template.semicolon.loadjs();


    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",

        sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        pageSize: 10,

        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller:{
            loadData: function(filter) {
                // var item = Session.get("nowrow");
                var p4_id = Session.get("nowrow_oproduct4");
                filter.carousel = "1";
                filter.oproduct4_id = p4_id;

                // var item = Session.get("nowrow_product");
                // filter.product1_id = item.product1_id;
                // filter.product2 = item.product2;
                // filter.product0 = 1;

                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [
            { type: "control", width:50,  },
            { name: "download", width:50, align:"center", title: "預覽",
                itemTemplate: function(value, item) {
                    // console.log(item);
                    // return '<button class="btn-file-download" data-id="'+item._id+'">預覽</button>';
                    return '<a href="'+item.url+'" target="_blank"><img class="grid-img" src="'+item.url+'"></a>';
                },
            },
            // { name: "uid", title: "#", width: 50 },
            // { name: "product1_id", title: "產品分類", type: "select", items: [{id:"",value:""}].concat(objProductClass), valueField: "id", textField: "value"},
            // { name: "providerid", title: "供應商", type: "select", items: [{_id:"",fullname:""}].concat(Provider.find().fetch()), valueField: "_id", textField: "fullname" },
            // { name: "product2", title: "產品代碼", type: "text" },
            { name: "name", title: "檔案名稱", type: "text" },
            { name: "title1", title: "圖片大標", type: "text" },
            { name: "title2", title: "圖片小標", type: "text" },
            // { name: "url", title: "圖片連結", type: "text" },
            // { name: "size", title: "檔案大小", type: "text" },
            { name: "createdByName", width:70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
        ],/*
        onDataLoading: function(args) {
        },
        onItemInserted: function(args) {
        },
        onItemUpdated: function(args) {
            console.log(args);
            $("#aProduct").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            // $("#aProduct").jsGrid("loadData");
        },*/
    });

    var ocPortfolio = $("#oc-portfolio");
    ocPortfolio.owlCarousel({
        margin: 20,
        nav: true,
        navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
        autoplay: false,
        autoplayHoverPause: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
};

Template.oproduct.helpers({
    // homepics: objHomePics
    Product1: Oproduct1.find(),
});

Template.oproduct.events({
    'click .my-open-editing': function() {
        var p4 = Session.get("nowrow_oproduct4");
        console.log(p4);
        $("#p4-title").html(p4.product4_text);
    },
    'click .btn-showlogin': function() {
    },
    'click #save-p4-title': function() {
        var item = Session.get("nowrow_oproduct4");
        var obj = {};

        obj.title1 = $("#title1").val() || "";
        obj.title2 = $("#title2").val() || "";
        Meteor.call("updateP4obj", item._id, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });
    },
    'click #add-p4-intro': function() {
        var str = $('<div class="col_one_fifth nobottommargin"> \
            <label for="intro_title">標題</label>\
        </div>\
        <div class="col_three_fifth nobottommargin">\
            <input name="intro_title[]" type="text" class="intro_title text sm-form-control">\
        </div>\
        <div class="col_one_fifth col_last nobottommargin">\
        </div>\
        <div class="clear"></div>\
        <div class="col_one_fifth nobottommargin">\
            <label for="intro_content[]">內文</label>\
        </div>\
        <div class="col_three_fifth col_last nobottommargin">\
            <textarea class="sm-form-control" name="intro_content[]" style="width:100%;height: 100px;"></textarea>\
        </div>\
        <div class="clear"></div>');
        $("#intro_others").append(str);
    },
    'click #save-p4-intro': function() {

      /*  var arr1 = $('input[name="intro_title[]"]').map(function () {
            // console.log(this.value);
            return this.value; // $(this).val()
        }).get();
        var arr2 = $('textarea[name="intro_content[]"]').map(function () {
            return this.value; // $(this).val()
        }).get();

        var arr = [];
        for(var i in arr1){
            if(arr1[i] || arr2[i]){
                var intro = {};
                intro.title = arr1[i];
                intro.content = arr2[i];
                arr.push(intro);
            }
        }
        var item = Session.get("nowrow_oproduct4");
        var obj = {};
        obj.intro = arr;
        Meteor.call("updateP4obj", item._id, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });*/

        var item = Session.get("nowrow_oproduct4");
        var obj = {};

        obj.intro_title1 = $("#intro_title1").val() || "";
        // obj.intro_content1 = $("#intro_content1").val() || "";
        obj.intro_title2 = $("#intro_title2").val() || "";
        // obj.intro_content2 = $("#intro_content2").val() || "";
        obj.intro_title3 = $("#intro_title3").val() || "";
        // obj.intro_content3 = $("#intro_content3").val() || "";

        obj.intro_content1 = CKEDITOR.instances["intro_content1"].getData() || "";
        obj.intro_content2 = CKEDITOR.instances["intro_content2"].getData() || "";
        obj.intro_content3 = CKEDITOR.instances["intro_content3"].getData() || "";

        Meteor.call("updateP4obj", item, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });
    },
    'click #save-p4-video': function() {
        var item = Session.get("nowrow_oproduct4");
        var obj = {};

        obj.youtube_url = $("#youtube_url").val() || "";
        obj.video_url   = $("#video_url").val() || "";

        obj.youtube_title = $("#youtube_title").val() || "";
        obj.youtube_content = $("#youtube_content").val() || "";
        obj.video_title = $("#video_title").val() || "";
        obj.video_content = $("#video_content").val() || "";
        Meteor.call("updateP4obj", item, obj, function(error, result){
            if(result){
                alert("已儲存");
            }
        });
    },
    // "change .myFileInput": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //                 var item = Session.get("nowrow_oproduct4");
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     oproduct4_id: item,
    //                     carousel: "0",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     // url: "/cfs/files/images/"+fileObj._id,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     // $("#cFiles").jsGrid("loadData");
    //
    //                     Meteor.call("updateP4Pic1", item, url, pic_id, function(error, result){
    //                         console.log(result);
    //                         Meteor.setTimeout(function(){
    //                             Template.oproduct.loadP4(result._id);
    //                             // $("#op4-big-pic").attr("src", url);
    //                         }, 3500);
    //                     });
    //                 });
    //             }
    //         });
    //     });
    // },
    "click a.upload1": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');
            var item = Session.get("nowrow_oproduct4");
            var formVar = {
                // email: "1",
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                oproduct4_id: item,
                carousel: "0",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
    "click a.upload2": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');
            var item = Session.get("nowrow_oproduct4");
            var formVar = {
                // email: "1",
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                oproduct4_id: item,
                carousel: "0",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
    "click a.upload3": function(){
        var files = $("input.file_bag")[0].files;
        var now_id = this._id;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            console.log(r);
            $('#input-3').fileinput('clear');
            var item = Session.get("nowrow_oproduct4");
            var formVar = {
                // email: "1",
                // name: fileObj.name(),
                // size: fileObj.size(),
                // file: fileObj,
                // image_id: fileObj._id,
                // url: "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name(),
                oproduct4_id: item,
                carousel: "1",
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,

                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
            $("#cFiles").jsGrid("insertItem", formVar);
        });
    },
    // "change .myFileInput2": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //                 var item = Session.get("nowrow_oproduct4");
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     oproduct4_id: item,
    //                     carousel: "0",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     // url: "/cfs/files/images/"+fileObj._id,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     Meteor.call("updateP4Pic2", item, url, pic_id, function(error, result){
    //                         $("#op4-big-pic2").attr("src", url);
    //                     });
    //                 });
    //             }
    //         });
    //     });
    // },
    // "change .myFileInput3": function(event, template) {
    //     // console.log("change .myFileInput");
    //     FS.Utility.eachFile(event, function(file) {
    //         Images.insert(file, function (err, fileObj) {
    //             if (err){
    //               console.log(err);
    //             }
    //             else {
    //                 // console.log(fileObj);
    //                 var item = Session.get("nowrow_oproduct4");
    //
    //                 var url = "https://s3-ap-northeast-1.amazonaws.com/hanbury/upload/images/"+fileObj._id+"-"+fileObj.name();
    //                 var pic_id = Uploads.insert({
    //                     oproduct4_id: item,
    //                     carousel: "1",
    //                     name: fileObj.name(),
    //                     size: fileObj.size(),
    //                     file: fileObj,
    //                     image_id: fileObj._id,
    //                     url: url,
    //                     // url: "/cfs/files/images/"+fileObj._id,
    //                     insertedById: Meteor.userId(),
    //                     insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
    //                     insertedAt: new Date()
    //                 }, function(err, file){
    //                     $("#cFiles").jsGrid("loadData");
    //                     // console.log(pic_id);
    //                 });
    //
    //             }
    //         });
    //     });
    // },
    "change select": function(event, template) {
        // console.log(event.currentTarget);
        // console.log(event.target);
        // console.log(template);
        if ($(event.target).hasClass("product")) {
            // $("#eproduct2").attr("disabled", true);
            // $("#eproduct3").attr("disabled", true);
            // $("#eproduct4").attr("disabled", true);

            var nowid = $(event.target).attr("id");
            if (nowid == "eproduct1") {
                $("#eproduct2").removeAttr("disabled");
                $("#eproduct3").attr("disabled", true);
                $("#eproduct4").attr("disabled", true);

                $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');

                var sel = $('#eproduct2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                var fieldtype = $('#eproduct1 :selected').val();

                var type = Oproduct2.find({
                    product1_id: fieldtype.toString()
                }, { sort: { product2_id: 1 } }).fetch();

                $.each(type, function(i, item2) {
                    sel.append($('<option>', {
                        value: item2._id,
                        text: item2.value
                    }));
                });
            } else if (nowid == "eproduct2") {
                $("#eproduct3").removeAttr("disabled");
                $("#eproduct4").attr("disabled", true);

                $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 無 --</option>');

                var fieldtype = $('#eproduct2 :selected').val();

                var type = Oproduct3.find({
                    product2_id: fieldtype.toString()
                }, { sort: { product3_id: 1 } }).fetch();

                if(!type.length){
                  $("#eproduct3").attr("disabled", true);
                  var sel = $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else{
                  $("#eproduct3").removeAttr("disabled");
                  var sel = $('#eproduct3')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                  $.each(type, function(i, item2) {
                      sel.append($('<option>', {
                          value: item2._id,
                          text: item2.value
                      }));
                  });
                }
            } else if (nowid == "eproduct3") {
                var fieldtype = $('#eproduct3 :selected').val();

                var type = Oproduct4.find({
                    product3_id: fieldtype.toString()
                }).fetch();

                if(!type.length){
                  $("#eproduct4").attr("disabled", true);
                  var sel = $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" disabled selected>-- 無 --</option>');
                }
                else{
                  $("#eproduct4").removeAttr("disabled");
                  var sel = $('#eproduct4')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="" selected>-- 請選擇 --</option>');

                  $.each(type, function(i, item2) {
                      sel.append($('<option>', {
                          value: item2._id,
                          text: item2.value_cht
                      }));
                  });
                }
            }
            else{
            }

            // console.log($(event.target));

            if (nowid != "eproduct4") {
                $("#p4").fadeIn("slow");
                $("#p4-management").hide();
            }
            else{
                var id = $(event.target).val();
                // console.log(id);
                Session.set("nowrow_oproduct4", id );
                if(id){
                    $("#to-outer-page").attr("href", "/product4/"+id);
                    $("#p4-management").fadeIn();
                    Template.oproduct.loadP4(id);
                }
            }
            $("#portfolio").jsGrid("loadData");
        }
    },
});
