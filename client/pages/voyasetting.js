Template.voyasetting.rendered = function(){
    Template.semicolon.loadjs();
};

Template.voyasetting.helpers({
  // homepics: objHomePics
});

Template.voyasetting.events({
  'click .btn-showlogin': function () {
    // Session.set('counter', Session.get('counter') + 1);
  },
});

Template.voyasetting.rendered = function(){
    Template.semicolon.loadjs();

    Session.set("workdays_type_id", "1");
    $("#workdays").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                filter.type_id = Session.get("workdays_type_id");

                return jsgridAjax(filter, "workdays", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "workdays", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "workdays", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "workdays", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, deleteButton: false },
            // { name: "uid", title: "#", width: 60},
            // { name: "isopen", type: "checkbox", title: "開啟" },
            // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
            // { name: "order_id", title: "排序", type: "text"},
            { name: "type_name", title: "類別", type: "text"},
            { name: "status", title: "Status", type: "text"},
            { name: "work_days", title: "預估工作天數", type: "text"},
            { name: "ps", title: "備註", type: "text"},
            // { name: "ps", title: "備註", type: "text", width: 200 },
            // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#workdays").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#workdays").jsGrid("loadData");
        },
    });
};
Template.voyasetting.helpers({
    get_countries: function() {
        return objCountries;
    },
});
Template.voyasetting.events({
    "change select": function(event, template) {
        // console.log(event.currentTarget);
        // console.log(event.target);
        // console.log(template);
        if ($(event.target).hasClass("type_id")) {
            var type_id = $("#type_id :selected").val();

            Session.set("workdays_type_id", type_id);
            $("#workdays").jsGrid("loadData");
        }
    },
});

