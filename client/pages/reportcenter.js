Template.reportcenter.rendered = function(){
    Template.semicolon.loadjs();
    //文件類別表單
    $("#e_reportcenter1").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                return jsgridAjax(filter, "reportcenter1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "reportcenter1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "reportcenter1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "reportcenter1", "DELETE");
            },
        },
        fields: [
            { type: "control", title: "刪除", width:40, },
            { name: "catalog", title: "文件類別", type: "text", width:100,},
            // { name: "blank_doc", title: "空白檔案", type: "text", width:80},
            // { name: "remark", title: "備註", type: "text", width:60 },
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_reportcenter1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_reportcenter1").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#e_reportcenter1 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortReportcenter", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_reportcenter1").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    //下方顯示表單
    $("#e_reportcenter2").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        // editing: false,
        loadIndication: true,
        // autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
        		if(!!Session.get('reportcenter1')){
	        		var f1 = Session.get('reportcenter1');
	        		filter.f1 = f1;
        		}
                return jsgridAjax(filter, "reportcenter2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "reportcenter2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "reportcenter2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "reportcenter2", "DELETE");
            },
        },
        fields: [
            { type: "control", title: "刪除", width:30, editButton: true },
            { name: "f1", title: "文件類別", type: "select",width:30, items: Reportcenter1.find({},{sort:{order_id: 1}}).fetch(), valueField: "_id", textField: "catalog"},
            { name: "doc_website", title: "標題", width:80,
            	itemTemplate: function(value, item) {
    			    var $text = $("<p>").text();
    			    var $link = $("<a>").attr("href", item.doc_website).attr("target", "_blank").text(item.doc_name);
			    return $("<div>").append($text).append($link);
				}
        	},
            { name: "remark", title: "備註", width:60, type:"text"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_reportcenter2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_reportcenter2").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#e_reportcenter2 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortReportcenter2", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_reportcenter2").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    //文件類別,切換顯示下方表單
    var val = $("#sel-catalog").find("option:selected").val();
    Session.set('reportcenter1', val);
    $("#e_reportcenter2").jsGrid("loadData");
    //文件類別管理,打開時顯示表單
    $('#myModal1').on('shown.bs.modal', function (event) {
        $("#e_reportcenter1").jsGrid("loadData");
    });
    //文件類別管理,關閉時清空input
    $('#myModal1').on('hide.bs.modal', function (event) {
        $("#catalog").val("");
    });
    //新增文件,打開時選好文件類別
    $('#myModal2').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $("#m2-f1").val( $("#sel-catalog option:selected").val() );
        $("#m2-f1_text").val( $("#sel-catalog option:selected").text() );
        $("#m2-f1_showtext").text( $("#sel-catalog option:selected").text() );
    });
    //新增文件,關閉時清空input
    $('#myModal2').on('hide.bs.modal', function (event) {
        $("#doc_name").val("");
        $("#doc_website").val("");
        $("#remark").val("");
    });
};

Template.reportcenter.helpers({
	get_catalog: Reportcenter1.find({}, {sort:{order_id: 1}}),
    get_catalog_count: function(){ return Reportcenter1.find().count(); },
});

Template.reportcenter.events({
	//切換類別,下方表單切換
	'change #sel-catalog': function(event) {
		// console.log(event);
		var val = $(event.currentTarget).find("option:selected").val();
        Session.set('reportcenter1', val);
        $("#e_reportcenter2").jsGrid("loadData");
    },
    //文件類別管理
    'submit .form-modal1': function(event) {
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal1").serializeObject();
        $("#e_reportcenter1").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                // $('#myModal1').modal('hide');
                $(".form-modal1").trigger('reset');
                $("#e_reportcenter1").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal1-error").text(ret);
            }
        });
    },
    //新增文件
    'submit .form-modal2': function(event) {
        $(".progress").hide();
        event.preventDefault();
        event.stopPropagation();
        var formVar = $(".form-modal2").serializeObject();
        $("#e_reportcenter2").jsGrid("insertItem", formVar).done(function(ret) {
            if (ret.insert == "success") {
                // $('#modal2').closeModal();
                $('#myModal2').modal('hide');
                $(".form-modal2").trigger('reset');
                $("#e_reportcenter2").jsGrid("loadData");
                // Materialize.toast('資料已新增', 3000, 'rounded');
            } else {
                $(".modal2-error").text(ret);
            }
        });
        // location.reload();
    },
});
