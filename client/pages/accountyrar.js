Template.accountyear.rendered = function(){
    Template.semicolon.loadjs();

    var data = this.data;
    // console.log(data);
    $("#title").text(data.value);
    Session.set("accountyear_bg", data);

    $("#e_accountyear").jsGrid({
        // height: "90%",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,

        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var bg = Session.get("accountyear_bg");
                filter.bg_id = bg._id;

                return jsgridAjax(filter, "accountyear", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "accountyear", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "accountyear", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "accountyear", "DELETE");
            },
        },
        fields: [
            // { type: "control", width:30, editButton: false },
            { type: "control", width:30},
            { name: "value", title: "會計年度", type: "text", align: "center"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_accountyear").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_accountyear").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#e_accountyear .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortAccountyear", items, function(error, result){
                        if(error){
                            console.log("error from Hrform: ", error);
                        }
                        else {
                            $("#e_accountyear").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
};

Template.accountyear.helpers({
});

Template.accountyear.events({
    'click .btn-new': function() {
        var formVar = {};
        var bg = Session.get("accountyear_bg");
        formVar.bg_id = bg._id;
        formVar.bg_text = bg.value;

        $("#e_accountyear").jsGrid("insertItem", formVar).done(function(ret) {
        });
    },
});