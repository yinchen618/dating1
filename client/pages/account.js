Template.account.rendered = function(){
    Template.semicolon.loadjs();

    var data = this.data;
    $("#title").text(data.value);
    Session.set("account_bg", data);
    $("#p2").hide();
    $("#p3").hide();

    $("#e_account1").jsGrid({
        // height: "90%",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var bg = Session.get("account_bg");
                filter.bg_id = bg._id;
                return jsgridAjax(filter, "account1", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "account1", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "account1", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "account1", "DELETE");
            },
        },
        fields: [
            { type: "control", width:30 },
            { name: "value", title: "會計科目", type: "text", align: "center"},
        ],
        onDataLoading: function(args) {
            // $("#e_account1").jsGrid("loadData");
        },
        onItemInserted: function(args) {
            $("#e_account1").jsGrid("loadData");
        },
        onItemUpdated: function(args) {
            $("#e_account1").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account1").jsGrid("loadData");
        },
        rowClick: function(args) {
            Session.set("nowrow_account1", args.item);
            $("#e_account2").jsGrid("loadData");
            $("#p2").fadeIn("slow");
            $("#a2").text(args.item.value);
        },
        onRefreshed: function() {
            var $gridData = $("#e_account1 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortAccount1", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct1: ", error);
                        }
                        else {
                            $("#e_account1").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#e_account2").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_account1");
                // filter.bg_id = bg._id;
                if(!!item && !!item._id){
                    // filter.product1_id = item._id;
                    filter.a1_id = item._id;
                }
                return jsgridAjax(filter, "account2", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "account2", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "account2", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "account2", "DELETE");
            },
        },
        fields: [
            { type: "control", width:30 },
            { name: "value", title: "會計科目", type: "text", align: "center"},
        ],
        onDataLoading: function(args) {
            // $("#e_account1").jsGrid("loadData");
        },
        onItemInserted: function(args) {
            $("#e_account2").jsGrid("loadData");
        },
        onItemUpdated: function(args) {
            $("#e_account2").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account2").jsGrid("loadData");
        },
        rowClick: function(args) {
            Session.set("nowrow_account2", args.item);
            $("#e_account3").jsGrid("loadData");
            $("#p3").fadeIn("slow");
            $("#a3").text(args.item.value);
        },
        onRefreshed: function() {
            var $gridData = $("#e_account2 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortAccount2", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct2: ", error);
                        }
                        else {
                            $("#e_account2").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
    $("#e_account3").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var item = Session.get("nowrow_account2");
                if(!!item && !!item._id){
                    // filter.product1_id = item._id;
                    filter.a2_id = item._id;
                }
                // var bg = Session.get("account_bg");
                // filter.bg_id = bg._id;
                return jsgridAjax(filter, "account3", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "account3", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "account3", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "account3", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40 },
            { name: "value", title: "科目細項", type: "text", align: "center"},
            { name: "ps", title: "說明", type: "text", align: "center"},
        ],
        onDataLoading: function(args) {
            // $("#e_account2").jsGrid("loadData");
        },
        onItemInserted: function(args) {
            $("#e_account3").jsGrid("loadData");
        },
        onItemUpdated: function(args) {
            $("#e_account3").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account3").jsGrid("loadData");
        },
       /* rowClick: function(args) {
            // Session.set("nowrow_account1", args.item);
            // $("#e_account2").jsGrid("loadData");
            // $("#p2").fadeIn("slow");
        },*/
        onRefreshed: function() {
            var $gridData = $("#e_account3 .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortAccount3", items, function(error, result){
                        if(error){
                            console.log("error from updateSortProduct3: ", error);
                        }
                        else {
                            $("#e_account3").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
};

Template.account.helpers({
});
Template.account.events({
    'click .btn-new1': function() {
        var formVar = {};
        var bg = Session.get("account_bg");
        formVar.bg_id = bg._id;
        formVar.bg_text = bg.value;

        $("#e_account1").jsGrid("insertItem", formVar).done(function(ret) {
            $("#e_account1").jsGrid("loadData");
        });
    },
    'click .btn-new2': function() {
        var formVar = {};
        // var bg = Session.get("nowrow_account1");
        // formVar.bg_id = bg._id;
        // formVar.bg_text = bg.value;
        var bg = Session.get("nowrow_account1");
        formVar.a1_id = bg._id;
        formVar.a1_text = bg.value;
        formVar.bg_id = bg.bg_id;
        formVar.bg_text = bg.bg_text;

        $("#e_account2").jsGrid("insertItem", formVar).done(function(ret) {
            $("#e_account2").jsGrid("loadData");
        });
    },
    'click .btn-new3': function() {
        console.log("new3 items");
        var formVar = {};
        var bg = Session.get("nowrow_account1");
        formVar.a1_id = bg._id;
        formVar.a1_text = bg.value;
        var bg = Session.get("nowrow_account2");
        formVar.a2_id = bg._id;
        formVar.a2_text = bg.value;
        formVar.bg_id = bg.bg_id;
        formVar.bg_text = bg.bg_text;

        $("#e_account3").jsGrid("insertItem", formVar).done(function(ret) {
            $("#e_account3").jsGrid("loadData");
        });
    },
});