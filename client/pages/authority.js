Template.authority.rendered = function(){
    Template.semicolon.loadjs();
    $("#authority").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "110%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                if(!filter.sortField){
                    filter.sortField = "order_id";
                    filter.sortOrder = "asc";
                }

                var sel_auth = $("#sel_filter").find("option:selected").val();
                if (sel_auth == "1" || !sel_auth) {
                    filter.is_auth = "1"
                } else if (sel_auth == "0") {
                    filter.is_auth = "0"
                } else if (sel_auth == "3") {}
                return jsgridAjax(filter, "authority", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "authority", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "authority", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "authority", "DELETE");
            },
        },
        fields: [
            { type: "control", width:60, deleteButton: false },
            { name: "worknumber", title: "工號", type: "text", editing:false, align: "center"},
            { name: "engname", title: "英文姓名", type: "text", editing:false, align: "center"},
            { name: "is_auth",  title: "登入功能", type: "select", items: objAuth0, valueField: "id", textField: "value"},
            { name: "auth_fin", title: "財務管理", type: "select", items: objAuth1, valueField: "id", textField: "value"},
            { name: "auth_hr",  title: "人資管理", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            { name: "auth_cus", title: "客戶管理", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            { name: "auth_as",  title: "售後服務", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            { name: "auth_prod", title: "產品中心", type: "select", items: objAuth2, valueField: "id", textField: "value"},
            { name: "auth_web", title: "網站管理", type: "select", items: objAuth2, valueField: "id", textField: "value"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#authority").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#authority").jsGrid("loadData");
        },
    });
};
Template.authority.helpers({
    get_countries: function() {
        return objCountries;
    },
});
Template.authority.events({
    "change #sel_filter": function(event, template) {
        $("#authority").jsGrid("loadData");
    },
});

