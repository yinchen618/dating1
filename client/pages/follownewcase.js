Template.follownewcase.rendered = function() {
  Template.semicolon.loadjs();
  var MyDateField = function(config) {
    jsGrid.Field.call(this, config);
  };

  MyDateField.prototype = new jsGrid.Field({
    sorter: function(date1, date2) {
      return new Date(date1) - new Date(date2);
    },

    itemTemplate: function(value) {
      return new Date(value).toDateString();
    },

    insertTemplate: function(value) {
      return this._insertPicker = $("<input>").datepicker({
        defaultDate: new Date()
      });
    },

    editTemplate: function(value) {
      return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },

    insertValue: function() {
      return this._insertPicker.datepicker("getDate").toISOString();
    },

    editValue: function() {
      return this._editPicker.datepicker("getDate").toISOString();
    }
  });

  jsGrid.fields.myDateField = MyDateField;

  $("#follownewcase").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    // sorting: true,
    paging: 10,
    // filtering: true,
    editing: true,
    loadIndication: true,
    autoload: true,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        var sa = $("#sel_agent").find("option:selected").val();
        if (sa != "-1") {
          filter.agent_id = sa
        };

        var st = $('#status_id').find("option:selected").val()
        if (st != "0") {
          filter.status = st
        };

        let td = $("#type_id").find("option:selected").val();
        filter.template_id = td;

        filter.follownewcase = "1";
        filter.nowphase = "1";
        return jsgridAjax(filter, "portfolio", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "portfolio", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "portfolio", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "portfolio", "DELETE");
      },
    },
    fields: [{
        type: "control",
        width: 80,
        deleteButton: false
      },
      {
        name: "name_cht",
        title: "Client",
        align: "center",
        width: 120,
        itemTemplate: function(value, item) {
          if (!!item.name_eng) {
            return '<a target="_blank" href="/client/' + item.client_id + '">' + item.name_cht + "<BR>" + item.name_eng + '</a>';
          } else if (!!item.name_cht) {
            return '<a target="_blank" href="/client/' + item.client_id + '">' + item.name_cht + '</a>';
          } else {
            return '<a target="_blank" href="/client/' + item.client_id + '">(名稱未填)</a>';
          }
        }
      },
      {
        name: "agent_text",
        title: "Agent",
        align: "center",
        width: 80
      },
      {
        name: "insertedAt",
        type: "myDateField",
        title: "Applied Date",
        align: "center",
        width: 95,
        itemTemplate: function(value, item) {
          return currentdate_eff(item.insertedAt);
        }
      },
      {
        name: "account_num",
        title: "Policy No.",
        align: "center",
        width: 100
      },
      {
        name: "product4_text",
        title: "投資項目",
        align: "center",
        width: 180,
        itemTemplate: function(value, item) {
          if (!!item.product4_text)
            return '<a target="_blank" href="/portfolio/' + item._id + '">' + item.product4_text + '</a>';
          return "";
        }
      },
      {
        name: "checkstatus",
        title: "進度",
        align: "center",
        width: 80,
        editing: false,
        itemTemplate: function(value, item) {
          // if(!!item.product4_text)
          return '<a target="#" data-toggle="modal" data-target="#myModal" data-pid="' + item._id + '">查看</a>';
          // return "";
        }
      },
      {
        name: "current_Status",
        title: "Current<br>Status",
        type: "select",
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item && !!item.current_Status && Workdays.find({_id: item.current_Status}).count())
            return Workdays.findOne({
              _id: item.current_Status
            }).status;
          return "";
        },
        editTemplate: function(value, item) {
          // console.log(item);
          var sel = $('<select class="my-current_Status"></select>')
            .find('option')
            .remove()
            .end();
          // .append('<option value="" disabled selected>請選擇</option>')
          ;
          var fieldtype = item.current_Status; //$(".account1_id-col select option:selected").val();
          var type = Workdays.find({
            type_id: $("#type_id").val()
          }, {
            sort: {
              order_id: 1
            }
          }).fetch();

          $.each(type, function(i, item2) {
            sel.append($('<option>', {
              value: item2._id,
              text: item2.status //+ " ($" + item2.price + ")",
            }));
          });

          if (!!value) {
            sel.val(value);
          }
          return this._editPicker = sel;
        },
        editValue: function() {
          return this._editPicker.val();
        }
      },
      {
        name: "current_status_days",
        title: "Current<br>Days",
        align: "center",
        width: 80,
        itemTemplate: function(value, item) {
          if (!!item && !!item.current_Status && Workdays.find({_id: item.current_Status}).count())
            return Workdays.findOne({
              _id: item.current_Status
            }).work_days;
          return ""
        },
      },
      {
        name: "next_Status",
        title: "Next<br>Status",
        type: "select",
        align: "center",
        itemTemplate: function(value, item) {
          if (!!item && !!item.next_Status && Workdays.find({_id: item.next_Status}).count())
            return Workdays.findOne({
              _id: item.next_Status
            }).status;
          return "";
        },
        editTemplate: function(value, item) {
          // console.log(item);
          var sel = $('<select class="my-next_Status"></select>')
            .find('option')
            .remove()
            .end();
          // .append('<option value="" disabled selected>請選擇</option>')
          ;
          var fieldtype = item.next_Status; //$(".account1_id-col select option:selected").val();
          var type = Workdays.find({
            type_id: $("#type_id").val()
          }, {
            sort: {
              order_id: 1
            }
          }).fetch();
          $.each(type, function(i, item2) {
            sel.append($('<option>', {
              value: item2._id,
              text: item2.status //+ " ($" + item2.price + ")",
            }));
          });
          if (!!value) {
            sel.val(value);
          }
          return this._editPicker = sel;
        },
        editValue: function() {
          return this._editPicker.val();
        }
      },
      {
        name: "status",
        title: "Status",
        type: "select",
        items: objASPhase,
        valueField: "id",
        textField: "value",
        width: 70,
        itemTemplate: function(value, item) {
          if (!!item && !!item.status) {
            return funcObjFind(objASPhase, value);
          } else {
            return "";
          }
        }
      },
      // { name: "next_status_days", title: "Next<br>Days", align: "center", width: 80,
      //     itemTemplate: function(value, item) {
      //         if(!!item && !!item.next_Status)
      //             return Workdays.findOne({_id: item.next_Status}).work_days;
      //         return ""
      //     },
      // },
      // { name: "FaceAmount", title: "Face<br>Amount", align: "center", width: 80},
      {
        name: "remark",
        title: "Remark",
        type: "text",
        align: "center",
        width: 80
      },
      {
        name: "owner",
        title: "Owner",
        type: "select",
        width: 80,
        items: objFollownewcaseOwner,
        valueField: "id",
        textField: "value"
      },
      {
        name: "record",
        title: "Record",
        align: "center",
        width: 80,
        type: "text"
      },
    ],
    onDataLoading: function(args) {
      // $('.jsgrid select').material_select();
    },
    onItemInserted: function(args) {
      // Materialize.toast('資料已新增!', 3000, 'rounded')
    },
    onItemUpdated: function(args) {
      // Materialize.toast('資料已更新', 3000, 'rounded')
      // console.log(args);
      $("#follownewcase").jsGrid("loadData");
    },
    onItemDeleted: function(args) {
      // Materialize.toast('資料已刪除', 3000, 'rounded')
      $("#follownewcase").jsGrid("loadData");
    },
  });
  $("#jsgPortfoliostatus").jsGrid({
    // height: "90%",
    // height: "500px",
    width: "100%",
    sorting: true,
    // paging: true,
    pageSize: 10000,
    // filtering: true,
    editing: false,
    loadIndication: true,
    autoload: false,
    pageLoading: true,
    loadMessage: "讀取中...",
    deleteConfirm: "確定要刪除此筆資料?",
    // data: "/api/provider",
    controller: {
      loadData: function(filter) {
        filter.portfolios_id = Session.get("portfoliostatus_pid");
        if (!filter.sortField) {
          filter.sortField = "insertedAt";
          filter.sortOrder = "desc";
        }
        return jsgridAjax(filter, "portfoliostatus", "GET");
      },
      insertItem: function(item) {
        return jsgridAjax(item, "portfoliostatus", "POST");
      },
      updateItem: function(item) {
        return jsgridAjax(item, "portfoliostatus", "PUT");
      },
      deleteItem: function(item) {
        return jsgridAjax(item, "portfoliostatus", "DELETE");
      },
    },
    fields: [
      // { type: "control", width:60, deleteButton: false },
      // { name: "uid", title: "#", width: 60},
      // { name: "isopen", type: "checkbox", title: "開啟" },
      // { name: "country", title: "地區", type: "number", type: "select", items: [{ id: "", value: "" }].concat(objCountries), valueField: "id", textField: "value", width: 100 },
      // { name: "order_id", title: "排序", type: "text"},
      {
        name: "status_text",
        title: "狀態",
        type: "text"
      },
      {
        name: "work_days",
        title: "實際/預估工作時數",
        type: "text",
        itemTemplate: function(value, item) {
          if (!!item.timediff) {
            return funcTimeInterval(item.timediff) + "/" + item.work_days + "日";
          } else {
            var diff = (new Date().getTime() - new Date(item.insertedAt).getTime()) / 1000;
            return funcTimeInterval(diff) + "/" + item.work_days + "日";
          }
          // if(!!item.insertedAt)
          //     return (new Date(item.insertedAt)).yyyymmddhm();
          // else
          //     return "";
        },
      },
      {
        name: "insertedAt",
        title: "開始時間",
        type: "text",
        itemTemplate: function(value, item) {
          if (!!item.insertedAt)
            return (new Date(item.insertedAt)).yyyymmddhm();
          else
            return "";
        },
      },
      {
        name: "closedAt",
        title: "結束時間",
        type: "text",
        itemTemplate: function(value, item) {
          if (!!item.closedAt)
            return (new Date(item.closedAt)).yyyymmddhm();
          else
            return "";
        },
      },
      // { name: "timediff", title: "實際工作時間", type: "text",
      //     itemTemplate: function(value, item) {
      //         if(!!item.timediff)
      //             return funcTimeInterval(item.timediff);
      //         else
      //              return "";
      //     },
      // }


      // { name: "ps", title: "備註", type: "text"},

      // { name: "ps", title: "備註", type: "text"},
      // { name: "ps", title: "備註", type: "text", width: 200 },
      // { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
    ],
    rowClass: function(item, itemIndex) {
      // console.log(item);

      var diff = parseInt((new Date() - new Date(item.insertedAt)) / 1000 / 60 / 60);
      if (!!item.closedAt) {
        diff = parseInt((new Date(item.closedAt) - new Date(item.insertedAt)) / 1000 / 60 / 60);
      }

      // console.log( "diff" );
      // console.log( diff );
      // console.log( "worked_hour" );
      var worked_hour = (item.work_days) * 8;
      // console.log( worked_hour );
      if (diff >= worked_hour) {
        // console.log( "row-danger" );
        return "row-danger";
      }
      // else if( diff >= work_days){
      //     return "row-warning";
      // }
    },
    /*  onDataLoading: function(args) {
          // $('.jsgrid select').material_select();
      },
      onItemInserted: function(args) {
          // Materialize.toast('資料已新增!', 3000, 'rounded')
      },
      onItemUpdated: function(args) {
          // Materialize.toast('資料已更新', 3000, 'rounded')
          // console.log(args);
          $("#workdays").jsGrid("loadData");
      },
      onItemDeleted: function(args) {
          // Materialize.toast('資料已刪除', 3000, 'rounded')
          $("#workdays").jsGrid("loadData");
      },*/
  });
  $('#myModal').on('show.bs.modal', function(event) {
    var btn = $(event.relatedTarget) // Button that triggered the modal
    // console.log(btn);
    Session.set("portfoliostatus_pid", $(btn).data("pid"));
    $("#jsgPortfoliostatus").jsGrid("loadData");
  });
  $("#csv_follownewcase").on('click', function(event) {
    var sel_text = $("#type_id").find("option:selected").text()
    var args = [$("#follownewcase"), '' + sel_text + '新建進度.csv'];
    exportTableToCSV.apply(this, args);
  });
  $("#csv_jsgPortfoliostatus").on('click', function(event) {
    var args = [$("#jsgPortfoliostatus"), '新建進度一覽.csv'];
    exportTableToCSV.apply(this, args);
  });
};
Template.follownewcase.helpers({
  get_agent: Agents.find(),
});
Template.follownewcase.events({
  'change #type_id': function(event) {
    $("#type_id").find("option:selected").val();
    $("#follownewcase").jsGrid("loadData");
  },
  'change #status_id': function(event) {
    $("#status_id").find("option:selected").val();
    $("#follownewcase").jsGrid("loadData");
  },
  'change #sel_agent': function(event) {
    $("#follownewcase").jsGrid("loadData");
  },
  'click .btn-sendEmails': function() {
    if ($("#sel_agent").val() == -1) {
      alert("請先選擇要寄送的Agent");
      return;
    }
    if (!confirm("確定寄送新件進度給Agent?")) {
      return;
    }
    var data = $("#follownewcase").jsGrid("option", "data");
    /*
    agent_text
    name_cht
    birthday
    */
    // console.log(data);

    $("#follownewcase2").html($("#follownewcase").html());

    $("#follownewcase2 .jsgrid-grid-body table").prepend("<tr class='jsgrid-header-row'>"+$("#follownewcase2 .jsgrid-grid-header table tr").html()+"</tr>");
    var row_html = $("#follownewcase2 .jsgrid-grid-body").html();

    // var table_html = $("#follownewcase .jsgrid-grid-header").html().replace(/(['])/g, "\\$1");
    // table_html = table_html + $("#follownewcase .jsgrid-grid-body").html().replace(/(['])/g, "\\$1");
    // table_html =  $(table_html).contents().unwrap().html();
    var table_html = '<base href="http://system.hanburyifa.com/" />' + row_html.replace(/<input class=\"jsgrid-button jsgrid-edit-button\" type=\"button\" title=\"編輯\">/g, "");

    Meteor.call("sendFollowNewCaseReminder", data, table_html, function(error, result) {
      if (error) {
        console.log("error from updateSortUploads: ", error);
      } else {
        alert("已成功送出新件進度清單\n" + result);
      }
    });
  },
});
