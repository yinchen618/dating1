Template.afterservice.rendered = function(){
    Template.semicolon.loadjs();
    // var $ = jQuery.noConflict();

    $("#cFiles").jsGrid({
        // height: "90%",
        width: "100%",
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        controller:{
            loadData: function(filter) {
                filter.afterservice = "1"; // 1:尚未送出 2: 已送出

                filter.sortField = "order_id";
                filter.sortOrder = "asc";

                return jsgridAjax(filter, "uploads", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "uploads", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "uploads", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "uploads", "DELETE");
            },
        },
        fields: [
            { type: "control", width:40, editButton:false},
            // { name: "order_id", title: "順序", type: "number", width: 60, editing: false },
            { name: "download", width:100, align:"center", title: "檔案",
                itemTemplate: function(value, item) {
                    return '<a href="#" data-toggle="modal" data-target="#myModalFile" data-url="'+item.url+'" data-index="'+item.myItemIndex+'">'+item.name+'</a>';
                },
            },
            // { name: "size", title: "檔案大小", type: "text", width:70, align:"right" },
            { name: "createdByName", width:70, title: "檔案建立者", type: "text" },
            { name: "insertedByName", width:70, title: "上傳者", type: "text", editing:false },
            { name: "insertedAt", title: "上傳日期", width:70,
                itemTemplate: function(value, item) {
                    if(!!item.insertedAt)
                        return (new Date(item.insertedAt)).yyyymmddhm();
                    else
                        return "";
                },
            },
        ],
        onRefreshed: function() {
            var $gridData = $("#cFiles .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    // arrays of items
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortUploads", items, function(error, result){
                        if(error){
                            console.log("error from updateSortUploads: ", error);
                        }
                        else {
                            $("#cFiles").jsGrid("loadData");
                        }
                    });
                }
            });
        },
    });
    $("#input-3").fileinput({
    	'language': "zh-TW",
    	'dropZoneEnabled': true
    });
    $('#myModalFile').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        // console.log(button);
        // console.log(button.data("id"));

        var index = button.data("index");
        var data = $("#cFiles").data("JSGrid").data[index];

        var url = button.data("url");
        $("#modal2-link").html( ' <a target="_blank" href="'+data.url+'">'+data.name +' (開新視窗)</a>');

        $("#file_iframe").attr("src", url);
    });
    $('#myModalFile').on('hide.bs.modal', function (event) {
        $("#file_iframe").attr("src", "");
    });
};

Template.afterservice.helpers({
  // homepics: objHomePics

    "files": function(){
        return S3.collection.find();
    }
});
Template.afterservice.events({
	'click .btn-showlogin': function () {
		// Session.set('counter', Session.get('counter') + 1);
	},
    // 'click .btn-print': function() {
    //     $("#file_iframe").get(0).contentWindow.print();
    // },
    "click a.upload": function(){
        var files = $("input.file_bag")[0].files;

        S3.upload({
                files:files,
                path:"subfolder"
        },function(e,r){
            // console.log(r);
            $('#input-3').fileinput('clear');

            var formVar = {
                afterservice: "1", // 1:尚未送出 2: 已送出
                name: r.file.original_name,
                size: r.file.size,
                file: r,
                image_id: r._id,
                url: r.secure_url,
                insertedById: Meteor.userId(),
                insertedByName: Meteor.user().profile.engname || Meteor.user().profile.chtname,
                insertedAt: new Date()
            };
        	$("#cFiles").jsGrid("insertItem", formVar);
        });
    },
});

