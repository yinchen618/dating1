Template.bankacc.rendered = function(){
    Template.semicolon.loadjs();

    var data = this.data;
    $("#title").text(data.value);
    Session.set("bankacc_bg", data);

    $("#bankacc").jsGrid({
        // height: "90%",
        // height: "500px",
        width: "100%",
        // sorting: true,
        paging: true,
        // filtering: true,
        editing: true,
        loadIndication: true,
        autoload: true,
        pageLoading: true,
        loadMessage: "讀取中...",
        deleteConfirm: "確定要刪除此筆資料?",
        // data: "/api/provider",
        controller: {
            loadData: function(filter) {
                var bg = Session.get("bankacc_bg");
                filter.bg_id = bg._id;
                filter.sortField = "order_id";
                filter.sortOrder = "asc";
                return jsgridAjax(filter, "bankacc", "GET");
            },
            insertItem: function(item) {
                return jsgridAjax(item, "bankacc", "POST");
            },
            updateItem: function(item) {
                return jsgridAjax(item, "bankacc", "PUT");
            },
            deleteItem: function(item) {
                return jsgridAjax(item, "bankacc", "DELETE");
            },
        },
        fields: [
            { type: "control", width:30 },
            { name: "value", title: "出款帳戶", type: "text", align: "center"},
        ],
        onDataLoading: function(args) {
            // $('.jsgrid select').material_select();
        },
        onItemInserted: function(args) {
            // Materialize.toast('資料已新增!', 3000, 'rounded')
        },
        onItemUpdated: function(args) {
            // Materialize.toast('資料已更新', 3000, 'rounded')
            // console.log(args);
            $("#e_account").jsGrid("loadData");
        },
        onItemDeleted: function(args) {
            // Materialize.toast('資料已刪除', 3000, 'rounded')
            $("#e_account").jsGrid("loadData");
        },
        onRefreshed: function() {
            var $gridData = $("#bankacc .jsgrid-grid-body tbody");
            $gridData.sortable({
                update: function(e, ui) {
                    var items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });
                    // console.log("Reordered items", items);
                    Meteor.call("updateSortBankacc", items, function(error, result){
                        if(error){
                            console.log("error from updateSortCounrty: ", error);
                        }
                        else {
                            $("#bankacc").jsGrid("loadData");
                        }
                    });
                }
            });
        }
    });
};

Template.bankacc.helpers({
    get_countries: function() {
        return objCountries;
    },
});

Template.bankacc.events({
    'click .btn-new': function() {
        var formVar = {};
        var bg = Session.get("bankacc_bg");
        formVar.bg_id = bg._id;
        formVar.bg_text = bg.value;

        $("#bankacc").jsGrid("insertItem", formVar).done(function(ret) {
        });
    },
});