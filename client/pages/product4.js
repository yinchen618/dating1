Template.product4.rendered = function() {
    Template.semicolon.loadjs();
    var controller = Router.current();
    var p4_id = controller.params.product4_id;
    var p3_id = Oproduct4.findOne({_id: p4_id}).product3_id;

    if(p3_id){
        var p4 = Oproduct4.find({
            product3_id: p3_id
        }, {sort:{order_id: 1}}).fetch();

        var dom = "";
        var dom2 = "";

        for(var i=0; i<p4.length; i++){
            data = p4[i];
            if(!!data.value_cht){
                if(p4_id == data._id){
                    dom = dom + '<li class="ui-tabs-active"><a class="p3-tab-href" data-id="'+data._id+'" href="/product4/'+data._id+'">'+data.value_cht+'</a></li>';
                }
                else{
                    dom = dom + '<li><a class="p3-tab-href" data-id="'+data._id+'" href="/product4/'+data._id+'">'+data.value_cht+'</a></li>';
                }
                dom2 = dom2 + '<div class="tab-content clearfix" id="tabs-'+data._id+'"></div>';
            }
        }
        $(".p3-ul").html(dom);
        $(".p3-tab-container").html(dom2);
    }


    // Tracker.autorun(function(){
    //     var data = Blaze.getData();


    // });
};
Template.product4.helpers({
    iscarousel: function() {
        return Uploads.find().count();
    },
});

Template.product4.events({
    'click .btn-showlogin': function() {
        // Session.set('counter', Session.get('counter') + 1);
    },
 /*   'click .p3-tab-href': function(event) {
        // Session.set('counter', Session.get('counter') + 1);
        // console.log("a");
        var p4_id = $(event.target).attr("data-id");
        // console.log(p4_id);
        Template.product4.loadP4(p4_id);
    },*/
});
